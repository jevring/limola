package ast;

/**
 * Represents a compound expression in Limola. This is an expression that consists of multiple operators,
 * for instance (1+2) == 3.
 * It consists of a left hand and a right hand side expression, which in turn can be nested to contain other
 * <code>CompoundExpression</code>s or <code>SimpleExpression</code>s.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-30 : 16:58:17
 */
public class CompoundExpression extends Expression {
    private Expression lh;
    private Expression rh;
    private int operator;


    public CompoundExpression(String statementDescription, Expression lh, Expression rh, int operator) {
        super(statementDescription);
        this.lh = lh;
        this.rh = rh;
        this.operator = operator;
        //System.out.println("Created compound expression: " + statementDescription);
    }

    public Expression getLh() {
        return lh;
    }

    public Expression getRh() {
        return rh;
    }

    public int getOperator() {
        return operator;
    }
}
