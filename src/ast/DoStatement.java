package ast;

import sse.StateSpaceExplorer;

import java.util.ArrayList;

/**
 * DO-version of the BranchingStatement.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-29 : 21:53:50
 */
public class DoStatement extends BranchingStatement {
    private ArrayList<Statement> breakStatements;

    public DoStatement(String statementDescription) {
        super(statementDescription);
        breakStatements = new ArrayList<Statement>();
        options = new ArrayList<Option>();
        //System.out.println("Created " + this.getClass() + ": " + statementDescription);
    }

    /**
     * The 'next statement' for a loop is either the beginning of the loop, or the statement that follows after the loop.
     * This method looks at all break statements in the loop and sets the 'next statement' of those break statements
     * to the statement after the loop. The looping functionality is handled when an option is added. (See #addOption(Option))
     *
     * @see #addOption(Option)
     * @param next the first statement after the loop.
     */
    public void setNextStatement(Statement next) {
        for (Statement s : breakStatements) {
            s.setNextStatement(next);
        }
    }

    /**
     * When adding an option to the loop, the 'next statement' for that option will be the beginning of the loop,
     * provided that the option added does not end in a Break, in which case the 'next statement'
     * will be set later (See #setNextStatement(Statement)), which will point to the first statement after the loop.
     *
     *
     * @param option one of the options in the loop.
     */
    public void addOption(Option option) {
        options.add(option);
        Statement s = option.getEnd();
        if (s instanceof Break) {
            breakStatements.add(s); // so that it can be set when we call next statement
        } else {
            option.setNextStatement(this); // this loops back to the beginning of the loop
        }
    }
}
