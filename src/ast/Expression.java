package ast;

/**
 * This is a superclass for all expressions in Limola.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-30 : 16:17:44
 */
public class Expression extends Statement {
    public Expression(String statementDescription) {
        super(statementDescription);
        //System.out.println("Created " + this.getClass() + ": " + statementDescription);
    }
}
