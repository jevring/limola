package ast;

/**
 * Represents an assignment operation in Limola.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-30 : 16:20:15
 */
public class Assignment extends Statement{
    private int variableIndex;
    private Expression value;


    public Assignment(String statementDescription, int variableIndex, Expression value) {
        super(statementDescription);
        this.variableIndex = variableIndex;
        this.value = value;
        //System.out.println("Created " + this.getClass() + ": " + statementDescription);
    }


    public int getVariableIndex() {
        return variableIndex;
    }

    public Expression getValue() {
        return value;
    }
}
