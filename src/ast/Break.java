package ast;

/**
 * Represents a break in Limola.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-29 : 23:27:02
 */
public class Break extends Statement {
    public Break(String statementDescription) {
        super(statementDescription);
        //System.out.println("Created " + this.getClass() + ": " + statementDescription);
    }
}
