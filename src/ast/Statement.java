package ast;

/**
 * Baseclass for all statements in Limola, regardless of their type.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-29 : 21:47:09
 */
public class Statement {
    private Statement nextStatement = null;
    private String description;
    private boolean endStatement;
    private int pc;
    private int pid;


    public Statement(String statementDescription) {
        this.description = statementDescription;
        //System.out.println("Created " + this.getClass() + ": " + statementDescription); // this might MEGA-spam, as this is called recursively. maybe we should just call this.
    }

    public Statement getNextStatement() {
        return nextStatement;
    }

    /**
     * Links this statement to the next one after it. This has different meanings depending on what type of statement it is.
     *
     * @param nextStatement the statement after this.
     */
    public void setNextStatement(Statement nextStatement) {
        this.nextStatement = nextStatement;
    }

    /**
     * Sets whether or not this is the end statement of a process.
     *
     * @param endStatement true if this is the last statement in the process, false otherwise.
     */
    public void setEndStatement(boolean endStatement) {
        this.endStatement = endStatement;
    }

    public boolean isEndStatement() {
        return endStatement;
    }

    /**
     * Returns the string representation of the statement. For branching statements, this will only contain the type,
     * such as "do" or "if", as the actual content of a brnaching statement is contained within its options.
     *
     * @return the string description of this statement.
     */
    public String getDescription() {
        return description;
    }

    public void setPc(int pc) {
        this.pc = pc;
    }

    public int getPc() {
        return pc;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getPid() {
        return pid;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
