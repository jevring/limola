package ast;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A representation of the specified Limola file. This model houses a mapping from variable names to state representation index,
 * as well as the processes contained in the model. Through these processes, the statements themselves are available.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-29 : 21:15:50
 */
public class Model {
    private int nextVariableId = 0;
    private HashMap<String, Integer> variables;
    private ArrayList<Process> processes;
    // this will contain a mapping from global variables (name)
    // and local variables (pid:name) to the id in which they can be found in
    // the state (array)
    // when a variable is encountered, it is replaced in the statement representation
    // with that id, so that lookups for the value in the state can be fast


    public Model() {
        variables = new HashMap<String, Integer>();
        processes = new ArrayList<Process>();
    }

    public void addGlobalVariable(String name) {
        //System.out.println("Added global variable " + name + " at index " + nextVariableId);
        variables.put(name, nextVariableId++);
    }

    public void addLocalVariable(int pid, String name) {
        //System.out.println("Added local variable " + pid + ":" + name + " at index " + nextVariableId);
        variables.put(pid + ":" + name, nextVariableId++);
    }

    public int getGlobalVariableIndex(String name) {
        Integer i = variables.get(name);
        if (i == null) {
            System.err.println("Trying to find global variable that doesn't exist. Are you referencing a local variable from another process? This can also happen if you put an assignment inside parentheses, which is a nono.");
            throw new IllegalArgumentException(name);
        }
        return i;
    }
    public int getLocalVariableIndex(int pid, String name) {
        Integer i = variables.get(pid + ":" + name);
        if (i == null) {
            System.err.println("Trying to find local variable that doesn't exist. Are you referencing a local variable from another process? This can also happen if you put an assignment inside parentheses, which is a nono.");
            throw new IllegalArgumentException(pid + ":" + name);
        }
        return i;
    }

    /**
     * Associates a process with this model.
     *
     * @param process the process to be included in the model.
     * @return the process id (pid) of the added process.
     */
    public int addProcess(Process process) {
        processes.add(process);
        process.setPid(processes.size() - 1); // the first process gets pid 0, etc
        return processes.size() - 1;
    }

    public ArrayList<Process> getProcesses() {
        return processes;
    }

    public Process getProcess(int pid) {
        return processes.get(pid);
    }

    public int getNumberOfProcesses() {
        return processes.size();
    }

    public int getNumberOfVariables() {
        //System.out.println(" there are " + (nextVariableId) + " variables");
        return nextVariableId; // don't do +1, since it has already been incremented when it was used to add a variable
    }

    /**
     * Checks the variable mapping to see if a variable referenced in a process is local or not.
     *
     * @param pid the process in which the variable was found.
     * @param name the name of the variable.
     * @return true if it is a variable local to the specified process, false otherwise.
     */
    public boolean isLocalVariable(int pid, String name) {
        return variables.containsKey(pid + ":" + name);
    }
}
