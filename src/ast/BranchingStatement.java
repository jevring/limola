package ast;

import java.util.ArrayList;

/**
 * Represents a branching statement in Limola, such as DO and IF.
 * This class contains the set of options that each branching statement has.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-31 : 00:04:26
 */
public class BranchingStatement extends Statement {
    protected ArrayList<Option> options = new ArrayList<Option>();

    public BranchingStatement(String statementDescription) {
        super(statementDescription);
    }

    public void addOption(Option option) {
        options.add(option);
    }

    public ArrayList<Option> getOptions() {
        return options;
    }

}
