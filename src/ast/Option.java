package ast;

/**
 * Represents the options present in all branching statements (do and if).
 * Each option has a start/guard and an end. If the guard is not executable, the rest of the statements in the option
 * are not executable either.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-29 : 22:19:49
 */
public class Option extends Statement {
    private Statement start; // this is the start.
    private Statement end; // this is the end
    // for a do, this will be connected to the beginning again
    // for an if, this will be connected to the beginning of the NEXT statement

    public Option(String statementDescription, Statement guard, Statement endOfOption) {
        super(statementDescription);
        this.start = guard;
        this.end = endOfOption;
        //System.out.println("Created " + this.getClass() + ": " + statementDescription);
    }

    /**
     * Connects the end of a statement chain in an option with the next statement. This is handled differently if the
     * Option is attatched to an If-statement of a Do-statement.
     *
     * @param nextStatement
     */
    public void setNextStatement(Statement nextStatement) {
        end.setNextStatement(nextStatement);
    }

    /**
     * This is the first statement in the chain of expressions for this option. If this is executable, the others become enabled. If not, this entire option branch is not enabled.
     * @return the guard of this option.
     */
    public Statement getGuard() {
        return start;
    }

    public Statement getEnd() {
        return end;
    }
}
