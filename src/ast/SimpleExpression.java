package ast;

/**
 * Represents a simple expression in Limola, such as a number or a variable. If it is a variable, it has a mapping to where
 * in the state representation its value can be found.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-30 : 16:37:25
 */
public class SimpleExpression extends Expression {
    private short value;
    private int variableIndex;
    private boolean isVariable = false;

    public SimpleExpression(String statementDescription) {
        super(statementDescription);
        //System.out.println("Created " + this.getClass() + ": " + statementDescription);
    }


    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public int getVariableIndex() {
        return variableIndex;
    }

    /**
     * Creates the mapping to where in the state representation this variable can be found.
     * @param variableIndex index in the state representation
     */
    public void setVariableIndex(int variableIndex) {
        this.variableIndex = variableIndex;
    }

    public boolean isVariable() {
        return isVariable;
    }

    public void setVariable(boolean variable) {
        isVariable = variable;
    }
}
