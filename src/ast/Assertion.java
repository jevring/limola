package ast;

/**
 * Represents an assertion in Limola.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-30 : 16:17:04
 */
public class Assertion extends Statement {
    private Expression containingExpression;

    public Assertion(String statementDescription) {
        super(statementDescription);
        //System.out.println("Created " + this.getClass() + ": " + statementDescription);
    }

    public void setContainingExpression(Expression containingExpression) {
        this.containingExpression = containingExpression;
    }

    public Expression getContainingExpression() {
        return containingExpression;
    }
}
