package ast;

/**
 * IF-version of the branching statement.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-29 : 22:24:37
 */
public class IfStatement extends BranchingStatement {

    public IfStatement(String statementDescription) {
        super(statementDescription);
        //System.out.println("Created " + this.getClass() + ": " + statementDescription);
    }

    /**
     * Sets the next statement for all options in this statement to the first statement after the if-statement.
     *
     * @param statement the first statement after this.
     */
    public void setNextStatement(Statement statement) {
        // connect the end of all option strings to this statement
        for (Option o : options) {
            o.getEnd().setNextStatement(statement);
        }
    }


}
