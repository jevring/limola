package ast;

import java.util.ArrayList;

/**
 * Represents a process in Limola. Contains statements, but no variables.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-29 : 21:32:09
 */
public class Process {
    private int pid;
    private ArrayList<Statement> statements;
    private String name;
    private Statement start;


    public Process(String name) {
        this.name = name;
        statements = new ArrayList<Statement>();
        //System.out.println("Creating Process: " + name);
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    /**
     * Indicates which statement this process should start with. This will normally be a special [start] statement.
     * Because we have this special start statement, the depth and state count of runs might appear slightly different,
     * compared to the same model being run in, for instance, spin.
     *
     * @param statement the statement that is first in this process.
     * @return the program counter that this statement receives.
     */
    public int setStartStatement(Statement statement) {
        start = statement;
        return addStatement(statement);
    }

    public Statement getStartStatement() {
        return start;
    }

    /**
     * Adds created statements to the process. For a statement to be executable, it needs to be linked in to a process,
     * as well has be linked as the 'next statement' from some other statement. (Except the start statement, which is
     * always executable, and does not have a 'previous' statement.
     *
     * @param statement the statement to add to the process.
     * @return the program counter that this statement got.
     */
    public int addStatement(Statement statement) {
        statements.add(statement);
        int pc = statements.size() - 1;
        statement.setPid(pid);
        statement.setPc(pc);

        // all processes are added to this list, so that they are easily accesible via an index, but they will also have
        // a link to the next statement (this link should include (or be replaced by) the index of the next statement
        return pc;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Statement> getStatements() {
        return statements;
    }

    public Statement getStatement(int pc) {
        return statements.get(pc);
    }
}
