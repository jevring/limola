// $ANTLR 2.7.7 (2006-11-01): "limola.g" -> "LimolaLexer.java"$

  package parser;

public interface LimolaTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int OPTION = 4;
	int EXPRESSION = 5;
	int ASSIGNMENT = 6;
	int DECLARATION = 7;
	int PROCESS = 8;
	int OREXPR = 9;
	int ANDEXPR = 10;
	int EQEXPR = 11;
	int RELEXPR = 12;
	int ADDEXPR = 13;
	int MULTEXPR = 14;
	int LITERAL_active = 15;
	int LITERAL_proctype = 16;
	int LPAREN = 17;
	int RPAREN = 18;
	int LBRACE = 19;
	int RBRACE = 20;
	int LITERAL_break = 21;
	int OR = 22;
	int AND = 23;
	int EQ = 24;
	int NOTEQ = 25;
	int LT = 26;
	int LTEQ = 27;
	int GT = 28;
	int GTEQ = 29;
	int PLUS = 30;
	int MINUS = 31;
	int MULT = 32;
	int DIV = 33;
	int MOD = 34;
	int NUMBER = 35;
	int ASSIGN = 36;
	int LITERAL_assert = 37;
	int LITERAL_if = 38;
	int LITERAL_fi = 39;
	int LITERAL_do = 40;
	int LITERAL_od = 41;
	int DOUBLECOLON = 42;
	int LITERAL_short = 43;
	int IDENTIFIER = 44;
	int SEMICOLON = 45;
	int ARROW = 46;
	int DIGIT = 47;
	int LETTER = 48;
	int NEWLINE = 49;
	int WS = 50;
	int SL_COMMENT = 51;
	int ML_COMMENT = 52;
}
