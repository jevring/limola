package parser;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-25 : 19:28:20
 */
public class UnsupportedOperatorException extends Exception {
    public UnsupportedOperatorException(int operator) {
        super("Unsupported operator: " + operator);
    }
}
