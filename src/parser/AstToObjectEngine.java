package parser;

import antlr.RecognitionException;
import antlr.TokenStreamException;
import antlr.collections.AST;
import ast.*;
import ast.Process;

import javax.tools.ToolProvider;
import javax.tools.JavaCompiler;
import java.io.*;
import java.util.ArrayList;

/**
 * Takes the AST generated from ANTlr and creates classes usable by the StateSpaceExplorer from it.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-29 : 21:35:45
 */
public class AstToObjectEngine {
    private AST ast;
	private String filename;
    private Model model;
    public boolean debug = false;

    public AstToObjectEngine(String filename) {
		this.filename = filename;
    }

    /**
     * Invokes the (generated) parser on the specified Limola file. Creates the model we will be using.
     * @param pcs a PrintWriter that poitns to a file that will contain the list of program counters for the different
     * statements in the various processes in the model.
     */
    public void parse(PrintWriter pcs) {
        if (pcs == null) {
            throw new IllegalArgumentException("You can't send in a PrintWriter for the program counters that is null");
        }
        BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(new File(filename)));
			LimolaLexer lexer = new LimolaLexer(in);
			LimolaParser parser = new LimolaParser(lexer);

            parser.model();
            ast = parser.getAST();
            if (debug) {
                prettyPrint(ast, 0);
            }

            generateObjectAbstractSyntaxTree();

/*
            ArrayList<Process> processes = model.getProcesses();
            for (Process p : processes) {
                System.out.println("process: " + p.getName());
                Statement statement = p.getStartStatement();
                System.out.println("Statements: {pid, description}");
                System.out.println("{" + statement.getPc() + ", " + statement.getDescription() + "}");
                Statement s0 = statement.getNextStatement();
                while (s0 != null) {
                    System.out.println("->{" + s0.getPc() + ", " + s0.getDescription() + "}");
                    if (s0 instanceof IfStatement) {
                        IfStatement s1 = (IfStatement)s0;
                        ArrayList<Option> options = s1.getOptions();
                        System.out.println("if[");
                        for (Option o : options) {
                            System.out.print("\t::option(");
                            Statement s2 = o.getGuard();
                            while (s2 != null) {
                                System.out.print("->{" + s2.getPc() + ", " + s2.getDescription() + "}");
                                s2 = s2.getNextStatement();
                            }
                            System.out.println(")");
                        }
                        System.out.print("]fi");
                    } else if (s0 instanceof DoStatement) {
                        DoStatement s1 = (DoStatement)s0;
                        ArrayList<Option> options = s1.getOptions();
                        System.out.println("do[");
                        for (Option o : options) {
                            System.out.print("\t::option(");
                            Statement s2 = o.getGuard();
                            while (s2 != null) {
                                System.out.print("->{" + s2.getPc() + ", " + s2.getDescription() + "}");
                                s2 = s2.getNextStatement();
                            }
                            System.out.println(")");
                        }
                        System.out.println("]od");
                    } 
                    s0 = s0.getNextStatement();
                }
                System.out.println("");
            }
*/
        //System.out.println("And linearly it looks weird. (but at least it shows that each statement has a unique pc)");
        for (Process p : model.getProcesses()) {
            if (debug) System.out.println("process: " + p.getName());
            pcs.println("process: " + p.getName());
            ArrayList<Statement> t = p.getStatements();
            for (int i = 0; i < t.size(); i++) {
                Statement statement = t.get(i);
                if (debug) System.out.println(statement.getPc() + ", " + statement.getDescription());
                pcs.println(statement.getPc() + ", " + statement.getDescription());
                //createAndCompileStatement(statement);
            }
        }
        pcs.flush();
        pcs.close();


        } catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (RecognitionException e) {
			e.printStackTrace();
		} catch (TokenStreamException e) {
			e.printStackTrace();
        } catch (IllegalGuardTypeException e) {
            System.err.println(e.getMessage());
        } 
    }

    private void createAndCompileStatement(Statement statement) {
        // read the file
        // replace the stuff
        // write it back to disk
        // compile
        // load
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("src/compiled/CompiledExpression.java")));
            String classFileName = "src/compiled/CompiledExpression_" + statement.getPid() + "_" + statement.getPc() + ".java";
            PrintWriter pr = new PrintWriter(new FileWriter(new File(classFileName)));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("1;//$$statement$$")) {
                    // special case for anything that isn't an expression (which means damn near everything)
                    pr.println(line.replace("1;//$$statement$$", statement.getDescription().replace("::", "") + ";"));
                } else if (line.contains("CompiledExpression")) {
                    pr.println(line.replace("CompiledExpression", "CompiledExpression_" + statement.getPid() + "_" + statement.getPc()));
                } else {
                    pr.println(line + "\r\n");
                }
            }
            pr.flush();
            pr.close();

            // generate all the files first, then we can worry about compiling straight from RAM
            //JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Ok, this is how it has to work. each statement has a (list of) link(s) to the NEXT statement(s) reachable from the current statement.
     * That way, when we're asking for successors, we'll just ask for the possible options (next statements, usually only one, except when we have branching statements).
     * When we have the 'next' statements, check if they are executable, and if so, create the state for them.
     */


    /**
     * This reads the AST and creates classes for use in the StateSpaceExplorer. It is invoked from the #parse() method.
     *
     * @throws IllegalGuardTypeException thrown if the type of a guard specified for some branching statement is illegal, for instance having another branching statement as the guard.
     */
    private void generateObjectAbstractSyntaxTree() throws IllegalGuardTypeException {
        // expressions can't contain assignments! (propagate this change through the code)
        // nor can assignments contain other assignments, this is very good!
        long start = System.currentTimeMillis();
        // uses private ast.
        model = new Model();

        // do the global declarations:
        AST child = ast;
        while (child != null) {
            if (child.getType() == LimolaTokenTypes.DECLARATION) {
                // declaration
                AST name = child.getFirstChild(); // variable name
                model.addGlobalVariable(name.getText());
            } else {
                // we're done with the declarations, this next has better be an "active proctype"
                break;
            }
            child = child.getNextSibling();
        }
        // do the active proctypes
        while (child != null) {
            if (child.getType() == LimolaTokenTypes.PROCESS) {
                createProcess(child);
            } else {
                break;  // what else can there be? (NOTHING, that's what!)
            }
            child = child.getNextSibling();
        }
        if (debug) {
            System.out.println("Time taken to generate OAST: "+ (System.currentTimeMillis() - start) + " milliseconds");
        }
    }

    /**
     * Creates the processes in the model.
     *
     * @param processNode the AST node to create the process from.
     * @throws IllegalGuardTypeException thrown if the type of a guard specified for some branching statement is illegal, for instance having another branching statement as the guard.
     */
    private void createProcess(AST processNode) throws IllegalGuardTypeException {
        Process process = new Process(processNode.getFirstChild().getText());
        int pid = model.addProcess(process);
        // do the declarations:
        AST child = processNode.getFirstChild().getNextSibling(); // gotta skip the name
        while (child != null) {
            if (child.getType() == LimolaTokenTypes.DECLARATION) {
                // declaration
                AST name = child.getFirstChild(); // variable name
                model.addLocalVariable(pid, name.getText());
            } else {
                // we're done with the declarations
                break;
            }
            child = child.getNextSibling();
        }
        if (child != null) {
            // continue with the statements;

            Statement start = new Statement("[start]");
            // note: all statements get added to the process, BUT NOT IN ORDER. We can still continue execution if we know the PC, since each statement knows which its next statement is!
            process.setStartStatement(start); // this ensures that all the following statements added to the start statement has an owner, which is this process
            Statement end = new Statement("[end]");
            process.addStatement(end);
            end.setEndStatement(true);
            Statement lastProcessStatement = start;
            while (child != null) {
                lastProcessStatement = processNode(child, lastProcessStatement, pid);
                process.addStatement(lastProcessStatement);
                lastProcessStatement.setNextStatement(lastProcessStatement);
                child = child.getNextSibling();
            }
            lastProcessStatement.setNextStatement(end);
        } // if child was null, we had no statements after the declatations

    }

    /**
     * Processes a single node, and returns the statement created from this node.
     * @param child the node from which to create the statement.
     * @param statementToConnectTheComingStatementsTo the statement to which we want to connect the node.
     * @param pid the process id of the process in which this statement recides.
     * @return the statement created from the supplied node.
     * @throws IllegalGuardTypeException thrown if the type of a guard specified for some branching statement is illegal, for instance having another branching statement as the guard.
     */
    public Statement processNode(AST child, Statement statementToConnectTheComingStatementsTo, int pid) throws IllegalGuardTypeException {
        //System.out.println("processing: " + child.getText() + " statementToConnectTheComingStatementsTo: " + statementToConnectTheComingStatementsTo.getDescription());
        //System.out.println(child.getText());

        Statement nextStatement = null;
        if (child.getType() == LimolaTokenTypes.LITERAL_do) {
            nextStatement = createDoStatement(child, pid);
        } else if (child.getType() == LimolaTokenTypes.LITERAL_if) {
            nextStatement = createIfStatement(child, pid);
        } else if (child.getType() == LimolaTokenTypes.LITERAL_assert) {
            nextStatement = createAssertion(child, pid);
        } else if (child.getType() == LimolaTokenTypes.ASSIGNMENT){
            nextStatement = createAssignment(child, pid);
        } else if (child.getType() == LimolaTokenTypes.EXPRESSION){
            nextStatement = createExpression(child, pid);
        } else if (child.getType() == LimolaTokenTypes.LITERAL_break){
            nextStatement = new Break(child.getText());
            // since we can't have a useful chain with any command after a break, signal the end of the loop
        }
        if (nextStatement != null) {
            //model.getProcess(pid).addStatement(nextStatement);
            statementToConnectTheComingStatementsTo.setNextStatement(nextStatement); // this means that the DO and IF-statements get their pointers set one iteration later, like everyone else

        } // else we found some object type that shouldn't be here, but the gammar should take care of that
        return nextStatement; // this is the last statement in the chain
    }

    // returns the last statement in the chain
    /**
     * Processes a while sequence of siblings in the AST.
     *
     * @param child the AST node to process.
     * @param statementToConnectTheComingStatementsTo the statement to connect this chain to.
     * @param pid the process id of the process in which this statement resides.
     * @return the LAST statement in the chain.
     * @throws IllegalGuardTypeException thrown if the type of a guard specified for some branching statement is illegal, for instance having another branching statement as the guard.
     */
    public Statement processSiblings(AST child, Statement statementToConnectTheComingStatementsTo, int pid) throws IllegalGuardTypeException {
        //System.out.println("processing: " + child.getText() + " statementToConnectTheComingStatementsTo: " + statementToConnectTheComingStatementsTo.getDescription());
        Statement previousStatement = statementToConnectTheComingStatementsTo;
        boolean breakEncountered = false;
        while (child != null) {
            //System.out.println(child.getText());


            Statement nextStatement = null;
            if (child.getType() == LimolaTokenTypes.LITERAL_do) {
                nextStatement = createDoStatement(child, pid);
            } else if (child.getType() == LimolaTokenTypes.LITERAL_if) {
                nextStatement = createIfStatement(child, pid);
            } else if (child.getType() == LimolaTokenTypes.LITERAL_assert) {
                nextStatement = createAssertion(child, pid);
            } else if (child.getType() == LimolaTokenTypes.ASSIGNMENT){
                nextStatement = createAssignment(child, pid);
            } else if (child.getType() == LimolaTokenTypes.EXPRESSION){
                nextStatement = createExpression(child, pid);
            } else if (child.getType() == LimolaTokenTypes.LITERAL_break){
                nextStatement = new Break(child.getText());
                // since we can't have a useful chain with any command after a break, signal the end of the loop
                breakEncountered = true;
            }
            if (nextStatement != null) {
                model.getProcess(pid).addStatement(nextStatement);
                previousStatement.setNextStatement(nextStatement); // this means that the DO and IF-statements get their pointers set one iteration later, like everyone else
                previousStatement = nextStatement;
            } // else we found some object type that shouldn't be here, but the gammar should take care of that
            if (breakEncountered) {
                break;
            }

/*            Statement nextStatement = processNode(child, statementToConnectTheComingStatementsTo, pid);
            if (nextStatement != null) {
                model.getProcess(pid).addStatement(nextStatement);
                previousStatement = nextStatement;
            } // else we found some object type that shouldn't be here, but the gammar should take care of that
            if (nextStatement instanceof Break) {
                break;
            }
*/            
            child = child.getNextSibling();
        }
        return previousStatement; // this is the last statement in the chain
    }

    /**
     * Creates a (nested) expression that represents a node. These can later be evaluated.
     *
     * @param expressionNode the node for which to create the expression.
     * @param pid the id of the process in which this expression resides.
     * @return the expression.
     */
    private Expression createExpression(AST expressionNode, int pid)  {
        // can come in as expression, operator or value/variable
        if (expressionNode.getType() == LimolaTokenTypes.EXPRESSION) {
            // go in once more, these were just parentheses
            //System.out.println("re-calling createExpression, and we are hoping that this is an operator: " + expressionNode.getFirstChild().getText() + " calling from " + expressionNode.getText());
            return createExpression(expressionNode.getFirstChild(), pid);
        } else {
            // create an expression based on what is in this thing.
            AST op = expressionNode;
            AST lh = expressionNode.getFirstChild();
            if (lh != null) {
                // then we actually had an operator with two evaluatable branches under it.
                AST rh = lh.getNextSibling();
                if (rh != null) {
                    Expression lhstat = createExpression(lh, pid);
                    int oper = op.getType();
                    Expression rhstat = createExpression(rh, pid);
                    String description = (lhstat instanceof SimpleExpression ? lhstat.getDescription() : "(" + lhstat.getDescription() + ")")
                                + " " + op.getText() + " "
                                + (rhstat instanceof SimpleExpression ? rhstat.getDescription() : "(" + rhstat.getDescription() + ")");
                    //System.out.println("Compound: " + lh.getText() + ":" + op.getText() + ":" + rh.getText());
                    return new CompoundExpression(description, lhstat, rhstat, oper);
                } else {
                    return createExpression(expressionNode, pid);
                }
                // no siblings means that this is the end of the line for this part of the expression

            } else {
                // no children, this means that it is a variable or a value
                SimpleExpression ss = new SimpleExpression(expressionNode.getText());
                if (expressionNode.getType() == LimolaTokenTypes.NUMBER) {
                    ss.setVariable(false);
                    short s = Short.parseShort(expressionNode.getText());
                    ss.setValue(s);
                } else {
                    ss.setVariable(true);
                    if (model.isLocalVariable(pid, expressionNode.getText())) {
                        ss.setVariableIndex(model.getLocalVariableIndex(pid, expressionNode.getText()));
                    } else {
                        ss.setVariableIndex(model.getGlobalVariableIndex(expressionNode.getText()));
                    }
                }
                return ss;
            }
        }
    }

    /**
     * Creates an assignment statement.
     *
     * @param assignmentNode
     * @param pid
     * @return
     */
    private Assignment createAssignment(AST assignmentNode, int pid)  {
        AST lh = assignmentNode.getFirstChild();
        // since this is an assignment, we KNOW that the LH is a variable. if it is not, throw an exception
        // or just assume that we don't get faulty programs
        int variableIndex;
        if (model.isLocalVariable(pid, lh.getText())) {
            variableIndex = model.getLocalVariableIndex(pid, lh.getText());
        } else {
            variableIndex = model.getGlobalVariableIndex(lh.getText());
        }
        Expression value = createExpression(lh.getNextSibling(), pid);
        return new Assignment(lh.getText() + " = " + value.getDescription(), variableIndex, value);
    }

    private Assertion createAssertion(AST assertionNode, int pid)  {
        Expression expression = createExpression(assertionNode.getFirstChild(), pid);
        Assertion assertion = new Assertion("assert(" + expression.getDescription() + ")");
        assertion.setContainingExpression(expression);
        return assertion;
    }

    private Statement createIfStatement(AST ifNode, int pid) throws IllegalGuardTypeException {
        IfStatement ifstat = new IfStatement(ifNode.getText());
        // - an if statement has different options
        // keep these options in a list of options
        AST optionNode = ifNode.getFirstChild();
        Option option;
        while (optionNode != null) {
            //System.out.println("Trying to make an option out of: " + optionNode.getText());
            option = createOption(optionNode, ifstat, pid);
            ifstat.addOption(option);
            // _todo: how do we bind the last part of this option chain to the statement AFTER the if?
            // this is done when the .setNextStatement() is invoked on the if-statement
            optionNode = optionNode.getNextSibling();
        }

        
        // - these options have a chain of statements, the first one being the guard (when executing, you see if the first command in the chain is executable or not)
        // keep this chain of statements in a LinkedList
        // at the end of this LinkedList is a special JUMP command that is always executable (and should ideally take no time to execute)
        // this JUMP command jumps to the END of the loop, or rather to the statement following the loop
        return ifstat;
    }

    /**
     * Creates an option chain for a specified branching statement. It will behave slightly differently if the branching
     * statement is a do or an if.
     *
     * @param optionNode the node from which to create the option.
     * @param branchingStatement used for linking the end of the option chain to something. In the case of a do, the option chain will be linked back to this statement.
     * @param pid
     * @return
     * @throws IllegalGuardTypeException
     */
    private Option createOption(AST optionNode, Statement branchingStatement, int pid) throws IllegalGuardTypeException {
        AST statement = optionNode.getFirstChild();
        boolean breakEncountered = false;
        Statement guard;
        // first get the guard.
        if (statement.getType() == LimolaTokenTypes.ASSIGNMENT) {
            guard = createAssignment(statement, pid);
        } else if (statement.getType() == LimolaTokenTypes.LITERAL_assert) {
            guard = createAssertion(statement, pid);
        } else if (statement.getType() == LimolaTokenTypes.LITERAL_break) {
            guard = new Break(statement.getText());
            breakEncountered = true;
        } else if (statement.getType() == LimolaTokenTypes.EXPRESSION) {
            guard = createExpression(statement, pid);
        } else {
            // this is a statement type we do not allow, throw an exception
            throw new IllegalGuardTypeException(statement);
        }
        guard.setDescription(":: " + guard.getDescription()); // the pc of the branching statement here will be 0, since it won't be added to the process until we return with all the options
        Statement endOfOption;
        if (!breakEncountered && statement.getNextSibling() != null) {
            // now that we have the guard, lets string together some more statements
            endOfOption = processSiblings(statement.getNextSibling(),guard, pid);
        } else {
            endOfOption = guard; // sets the break statement as the end of this guard
        }
        model.getProcess(pid).addStatement(guard);
        Option option = new Option(guard.getDescription(), guard, endOfOption);

        if (branchingStatement instanceof DoStatement) {
            // we have a do statement, and we need to link this back to the beginning, PROVIDED that it is not a break
            if (endOfOption instanceof Break) {
            } else {
                endOfOption.setNextStatement(branchingStatement); // hmm, this might be done twice now, but I guess that's ok.
            }
        }

        // NO, don't keep these in a LinkedList. The statements themselves need to know what the next statment is
        // - these options have a chain of statements, the first one being the guard (when executing, you see if the first command in the chain is executable or not)
        // keep this chain of statements in a LinkedList
        // at the end of this LinkedList is a special JUMP command that is always executable (and should ideally take no time to execute)
        // this JUMP command jumps to the END of the loop, or rather to the statement following the loop
        return option;
    }

    private DoStatement createDoStatement(AST doNode, int pid) throws IllegalGuardTypeException {
        DoStatement dostat = new DoStatement(doNode.getText());
        // - an if statement has different options
        // keep these options in a list of options
        AST optionNode = doNode.getFirstChild();
        Option option;
        while (optionNode != null) {
            option = createOption(optionNode, dostat, pid);
            dostat.addOption(option);
            // _todo: how do we bind the last part of this option chain to the statement AFTER the if?
            // this is done when the .setNextStatement() is invoked on the if-statement
            optionNode = optionNode.getNextSibling();
        }

        return dostat;
        // - a do statement has different options.
        // keep these options in a list of options
        // - these options have a chain of statements, the first one being the guard (when executing, you see if the first command in the chain is executable or not)
        // keep this chain of statements in a LinkedList
        // at the end of this LinkedList is a special JUMP command that is always executable (and should ideally take no time to execute)
        // this JUMP command jumps back to the BEGINNING of the loop
    }

    public Model getModel() {
        return model;
    }

	public static void printSpaces(int n)
	{
		for (int i = 0; i < n; i++)
			System.out.print(" ");
	}

    /**
     * Helps print the AST in a human-readable way.
     * @param ast
     * @param indentLevel
     */
    public static void prettyPrint(AST ast, int indentLevel)
	{
		printSpaces(indentLevel);
		System.out.println(/*ast.getType() + " : " + */ast.getText() +" (" + ast.getNumberOfChildren() + ")");
		if (ast.getFirstChild() != null)
			prettyPrint(ast.getFirstChild(), indentLevel + 2);
		if (ast.getNextSibling() != null)
			prettyPrint(ast.getNextSibling(), indentLevel);
	}
    
}
