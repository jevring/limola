package parser;

import antlr.collections.AST;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-30 : 00:25:22
 */
public class IllegalGuardTypeException extends Throwable {
    public IllegalGuardTypeException(AST statement) {
        super("Illegal guard type found: " + statement.getText());
    }
}
