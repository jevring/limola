header
{
  package parser;
}

class LimolaParser extends Parser;
options
{
  exportVocab = Limola;
  buildAST = true;
  k = 2; // Lookahead. Rule of thumb: max. 2.
}

tokens {
OPTION;
EXPRESSION;
ASSIGNMENT;
DECLARATION;
PROCESS;
OREXPR;
ANDEXPR;
EQEXPR;
RELEXPR;
ADDEXPR;
MULTEXPR;
}


model : declarations processes;
declarations : (declaration terminator)*;
declaration : type! variable{
    #declaration = #([DECLARATION, "declaration"], #declaration);
};

processes : (process)+;
process : "active"! "proctype"! variable LPAREN! RPAREN! LBRACE! body RBRACE!{
    #process = #([PROCESS, "process"], #process);
};
body : declarations statements;
statements : (statement terminator)+;
statement : expression
			| assignment
			| assertion
			| ifstat
			| dostat
			| "break";
expression : orexpr {
    #expression = #([EXPRESSION, "expression"], #expression);
};
orexpr : andexpr (OR^ andexpr)*;
andexpr : eqexpr (AND^ eqexpr)*;
eqexpr : relexpr ((EQ^ | NOTEQ^) relexpr)*;
relexpr : addexpr ((LT^ | LTEQ^ | GT^ | GTEQ^) addexpr)*;
addexpr : multexpr ((PLUS^ | MINUS^) multexpr)*;
multexpr : (primexpr) ((MULT^ | DIV^ | MOD^) (primexpr))*;
/*
expression : orexpr {
    #expression = #([EXPRESSION, "expression"], #expression);
};
orexpr : andexpr (OR^ andexpr)*{
    #orexpr = #([OREXPR, "orexpr"], #orexpr);
};
andexpr : eqexpr (AND^ eqexpr)*{
    #andexpr = #([ANDEXPR, "andexpr"], #andexpr);
};
eqexpr : relexpr ((EQ^ | NOTEQ^) relexpr)* {
    #eqexpr = #([EQEXPR, "eqexpr"], #eqexpr);
};
relexpr : addexpr ((LT^ | LTEQ^ | GT^ | GTEQ^) addexpr)* {
    #relexpr = #([RELEXPR, "relexpr"], #relexpr);
};
addexpr : multexpr ((PLUS^ | MINUS^) multexpr)* {
    #addexpr = #([ADDEXPR, "addexpr"], #addexpr);
};
multexpr : (primexpr) ((MULT^ | DIV^ | MOD^) (primexpr))* {
    #multexpr = #([MULTEXPR, "multexpr"], #multexpr);
};
*/
primexpr : t1 | LPAREN! expression RPAREN!;
t1 : variable | NUMBER;

// (true && false) || true -> true
// true && (false || true) -> true

// 1 *2+1*2==1
// 1 * (2+1) * 2 == 1

/*

expression : t (op t)* {
    #expression = #([EXPRESSION, "expression"], #expression);
};
t : LPAREN! expression RPAREN! | t1;
t1 : variable | NUMBER;

op : multop | addop | eqop | boolop;
*/

boolop : OR | AND;
eqop : EQ | NOTEQ | LT | LTEQ | GT | GTEQ;
addop : MINUS | PLUS;
multop : MULT | DIV | MOD;

assignment : variable ASSIGN! expression{
    #assignment = #([ASSIGNMENT, "assignment"], #assignment);
};
assertion : "assert"^ LPAREN! expression RPAREN!;
ifstat : "if"^ (option)+ "fi"!;
dostat : "do"^ (option)+ "od"!;
option : DOUBLECOLON! guard terminator (statement terminator)* {
    #option = #([OPTION, "option"], #option);
};
guard : expression | assignment | assertion | "break" ; // Assignment, assertion & break are always executable
type : "short";
variable : IDENTIFIER;
terminator : SEMICOLON! | ARROW!;
//number : NUMBER;


// Lexer part:

class LimolaLexer extends Lexer;
options
{
  k = 2;
  exportVocab = Limola;
}

// protected token types are only used to define other tokens; the lexer will
// never return tokens of 'protected' types.
protected DIGIT         : '0'..'9' ;
protected LETTER        : 'a'..'z' | 'A'..'Z' ;
protected NEWLINE       : (("\r\n") => "\r\n"           //DOS
                          | '\r'                        //Macintosh
                          | '\n'){newline();};          //Unix
//protected UNDERSCORE    : '_';

LBRACE    	: '{';
RBRACE    	: '}';
LPAREN    	: '(';
RPAREN    	: ')';
SEMICOLON 	: ';';
LT        	: '<';
GT        	: '>';
MULT      	: '*';
ASSIGN 	  	: '=';
DIV	  	    : '/';
MOD	  	    : '%';
PLUS	    : '+';
MINUS	    : '-';
ARROW     	: "->";
AND	  	  	: "&&";
OR	  	  	: "||";
GTEQ	  	: ">=";
LTEQ	  	: "<=";
NOTEQ	  	: "!=";
EQ	  	  	: "==";
DOUBLECOLON : "::";

//COLON     : ':';
//DOT       : '.';
//COMMA     : ',';
//NOT       : '!';
//OR        : '|';
//AND       : '&';


IDENTIFIER : LETTER (LETTER | DIGIT)*;
NUMBER: (DIGIT)+;

//COMMENT		: "/*" ( options {greedy=false;} : . )* "*/" {$channel=HIDDEN;};

// Ignore whitespace (be careful when you add string-literals...)
WS         : (' ' | '\t' | '\f' | NEWLINE) { $setType(Token.SKIP); };


// Comment rules stolen from Java 1.5 grammar.
// Single-line comments
SL_COMMENT
	:	"//"
		(~('\n'|'\r'))* ('\n'|'\r'('\n')?)
		{$setType(Token.SKIP); newline();}
	;

// multiple-line comments
ML_COMMENT
	:	"/*"
		(	/*	'\r' '\n' can be matched in one alternative or by matching
				'\r' in one iteration and '\n' in another. I am trying to
				handle any flavor of newline that comes in, but the language
				that allows both "\r\n" and "\r" and "\n" to all be valid
				newline is ambiguous. Consequently, the resulting grammar
				must be ambiguous. I'm shutting this warning off.
			 */
			options {
				generateAmbigWarnings=false;
			}
		:
			{ LA(2)!='/' }? '*'
		|	'\r' '\n'		{newline();}
		|	'\r'			{newline();}
		|	'\n'			{newline();}
		|	~('*'|'\n'|'\r')
		)*
		"*/"
		{$setType(Token.SKIP);}
	;