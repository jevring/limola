// $ANTLR 2.7.7 (2006-11-01): "limola.g" -> "LimolaParser.java"$

  package parser;

import antlr.*;
import antlr.collections.AST;
import antlr.collections.impl.ASTArray;
import antlr.collections.impl.BitSet;

public class LimolaParser extends antlr.LLkParser       implements LimolaTokenTypes
 {

protected LimolaParser(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

public LimolaParser(TokenBuffer tokenBuf) {
  this(tokenBuf,2);
}

protected LimolaParser(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

public LimolaParser(TokenStream lexer) {
  this(lexer,2);
}

public LimolaParser(ParserSharedInputState state) {
  super(state,2);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

	public final void model() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST model_AST = null;
		
		try {      // for error handling
			declarations();
			astFactory.addASTChild(currentAST, returnAST);
			processes();
			astFactory.addASTChild(currentAST, returnAST);
			model_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
		returnAST = model_AST;
	}
	
	public final void declarations() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST declarations_AST = null;
		
		try {      // for error handling
			{
			_loop4:
			do {
				if ((LA(1)==LITERAL_short)) {
					declaration();
					astFactory.addASTChild(currentAST, returnAST);
					terminator();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop4;
				}
				
			} while (true);
			}
			declarations_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_1);
		}
		returnAST = declarations_AST;
	}
	
	public final void processes() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST processes_AST = null;
		
		try {      // for error handling
			{
			int _cnt8=0;
			_loop8:
			do {
				if ((LA(1)==LITERAL_active)) {
					process();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					if ( _cnt8>=1 ) { break _loop8; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt8++;
			} while (true);
			}
			processes_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
		returnAST = processes_AST;
	}
	
	public final void declaration() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST declaration_AST = null;
		
		try {      // for error handling
			type();
			variable();
			astFactory.addASTChild(currentAST, returnAST);
			declaration_AST = (AST)currentAST.root;
			
			declaration_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(DECLARATION,"declaration")).add(declaration_AST));
			
			currentAST.root = declaration_AST;
			currentAST.child = declaration_AST!=null &&declaration_AST.getFirstChild()!=null ?
				declaration_AST.getFirstChild() : declaration_AST;
			currentAST.advanceChildToEnd();
			declaration_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		returnAST = declaration_AST;
	}
	
	public final void terminator() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST terminator_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case SEMICOLON:
			{
				match(SEMICOLON);
				terminator_AST = (AST)currentAST.root;
				break;
			}
			case ARROW:
			{
				match(ARROW);
				terminator_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		returnAST = terminator_AST;
	}
	
	public final void type() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST type_AST = null;
		
		try {      // for error handling
			AST tmp3_AST = null;
			tmp3_AST = astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp3_AST);
			match(LITERAL_short);
			type_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_4);
		}
		returnAST = type_AST;
	}
	
	public final void variable() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST variable_AST = null;
		
		try {      // for error handling
			AST tmp4_AST = null;
			tmp4_AST = astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp4_AST);
			match(IDENTIFIER);
			variable_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_5);
		}
		returnAST = variable_AST;
	}
	
	public final void process() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST process_AST = null;
		
		try {      // for error handling
			match(LITERAL_active);
			match(LITERAL_proctype);
			variable();
			astFactory.addASTChild(currentAST, returnAST);
			match(LPAREN);
			match(RPAREN);
			match(LBRACE);
			body();
			astFactory.addASTChild(currentAST, returnAST);
			match(RBRACE);
			process_AST = (AST)currentAST.root;
			
			process_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(PROCESS,"process")).add(process_AST));
			
			currentAST.root = process_AST;
			currentAST.child = process_AST!=null &&process_AST.getFirstChild()!=null ?
				process_AST.getFirstChild() : process_AST;
			currentAST.advanceChildToEnd();
			process_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_6);
		}
		returnAST = process_AST;
	}
	
	public final void body() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST body_AST = null;
		
		try {      // for error handling
			declarations();
			astFactory.addASTChild(currentAST, returnAST);
			statements();
			astFactory.addASTChild(currentAST, returnAST);
			body_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_7);
		}
		returnAST = body_AST;
	}
	
	public final void statements() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST statements_AST = null;
		
		try {      // for error handling
			{
			int _cnt13=0;
			_loop13:
			do {
				if ((_tokenSet_8.member(LA(1)))) {
					statement();
					astFactory.addASTChild(currentAST, returnAST);
					terminator();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					if ( _cnt13>=1 ) { break _loop13; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt13++;
			} while (true);
			}
			statements_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_7);
		}
		returnAST = statements_AST;
	}
	
	public final void statement() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST statement_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LITERAL_assert:
			{
				assertion();
				astFactory.addASTChild(currentAST, returnAST);
				statement_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_if:
			{
				ifstat();
				astFactory.addASTChild(currentAST, returnAST);
				statement_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_do:
			{
				dostat();
				astFactory.addASTChild(currentAST, returnAST);
				statement_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_break:
			{
				AST tmp11_AST = null;
				tmp11_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp11_AST);
				match(LITERAL_break);
				statement_AST = (AST)currentAST.root;
				break;
			}
			default:
				if ((LA(1)==LPAREN||LA(1)==NUMBER||LA(1)==IDENTIFIER) && (_tokenSet_9.member(LA(2)))) {
					expression();
					astFactory.addASTChild(currentAST, returnAST);
					statement_AST = (AST)currentAST.root;
				}
				else if ((LA(1)==IDENTIFIER) && (LA(2)==ASSIGN)) {
					assignment();
					astFactory.addASTChild(currentAST, returnAST);
					statement_AST = (AST)currentAST.root;
				}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		returnAST = statement_AST;
	}
	
	public final void expression() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST expression_AST = null;
		
		try {      // for error handling
			orexpr();
			astFactory.addASTChild(currentAST, returnAST);
			expression_AST = (AST)currentAST.root;
			
			expression_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(EXPRESSION,"expression")).add(expression_AST));
			
			currentAST.root = expression_AST;
			currentAST.child = expression_AST!=null &&expression_AST.getFirstChild()!=null ?
				expression_AST.getFirstChild() : expression_AST;
			currentAST.advanceChildToEnd();
			expression_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_10);
		}
		returnAST = expression_AST;
	}
	
	public final void assignment() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST assignment_AST = null;
		
		try {      // for error handling
			variable();
			astFactory.addASTChild(currentAST, returnAST);
			match(ASSIGN);
			expression();
			astFactory.addASTChild(currentAST, returnAST);
			assignment_AST = (AST)currentAST.root;
			
			assignment_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(ASSIGNMENT,"assignment")).add(assignment_AST));
			
			currentAST.root = assignment_AST;
			currentAST.child = assignment_AST!=null &&assignment_AST.getFirstChild()!=null ?
				assignment_AST.getFirstChild() : assignment_AST;
			currentAST.advanceChildToEnd();
			assignment_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		returnAST = assignment_AST;
	}
	
	public final void assertion() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST assertion_AST = null;
		
		try {      // for error handling
			AST tmp13_AST = null;
			tmp13_AST = astFactory.create(LT(1));
			astFactory.makeASTRoot(currentAST, tmp13_AST);
			match(LITERAL_assert);
			match(LPAREN);
			expression();
			astFactory.addASTChild(currentAST, returnAST);
			match(RPAREN);
			assertion_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		returnAST = assertion_AST;
	}
	
	public final void ifstat() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST ifstat_AST = null;
		
		try {      // for error handling
			AST tmp16_AST = null;
			tmp16_AST = astFactory.create(LT(1));
			astFactory.makeASTRoot(currentAST, tmp16_AST);
			match(LITERAL_if);
			{
			int _cnt50=0;
			_loop50:
			do {
				if ((LA(1)==DOUBLECOLON)) {
					option();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					if ( _cnt50>=1 ) { break _loop50; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt50++;
			} while (true);
			}
			match(LITERAL_fi);
			ifstat_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		returnAST = ifstat_AST;
	}
	
	public final void dostat() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST dostat_AST = null;
		
		try {      // for error handling
			AST tmp18_AST = null;
			tmp18_AST = astFactory.create(LT(1));
			astFactory.makeASTRoot(currentAST, tmp18_AST);
			match(LITERAL_do);
			{
			int _cnt53=0;
			_loop53:
			do {
				if ((LA(1)==DOUBLECOLON)) {
					option();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					if ( _cnt53>=1 ) { break _loop53; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt53++;
			} while (true);
			}
			match(LITERAL_od);
			dostat_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		returnAST = dostat_AST;
	}
	
	public final void orexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST orexpr_AST = null;
		
		try {      // for error handling
			andexpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop18:
			do {
				if ((LA(1)==OR)) {
					AST tmp20_AST = null;
					tmp20_AST = astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp20_AST);
					match(OR);
					andexpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop18;
				}
				
			} while (true);
			}
			orexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_10);
		}
		returnAST = orexpr_AST;
	}
	
	public final void andexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST andexpr_AST = null;
		
		try {      // for error handling
			eqexpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop21:
			do {
				if ((LA(1)==AND)) {
					AST tmp21_AST = null;
					tmp21_AST = astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp21_AST);
					match(AND);
					eqexpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop21;
				}
				
			} while (true);
			}
			andexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_11);
		}
		returnAST = andexpr_AST;
	}
	
	public final void eqexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST eqexpr_AST = null;
		
		try {      // for error handling
			relexpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop25:
			do {
				if ((LA(1)==EQ||LA(1)==NOTEQ)) {
					{
					switch ( LA(1)) {
					case EQ:
					{
						AST tmp22_AST = null;
						tmp22_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp22_AST);
						match(EQ);
						break;
					}
					case NOTEQ:
					{
						AST tmp23_AST = null;
						tmp23_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp23_AST);
						match(NOTEQ);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					relexpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop25;
				}
				
			} while (true);
			}
			eqexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_12);
		}
		returnAST = eqexpr_AST;
	}
	
	public final void relexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST relexpr_AST = null;
		
		try {      // for error handling
			addexpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop29:
			do {
				if (((LA(1) >= LT && LA(1) <= GTEQ))) {
					{
					switch ( LA(1)) {
					case LT:
					{
						AST tmp24_AST = null;
						tmp24_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp24_AST);
						match(LT);
						break;
					}
					case LTEQ:
					{
						AST tmp25_AST = null;
						tmp25_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp25_AST);
						match(LTEQ);
						break;
					}
					case GT:
					{
						AST tmp26_AST = null;
						tmp26_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp26_AST);
						match(GT);
						break;
					}
					case GTEQ:
					{
						AST tmp27_AST = null;
						tmp27_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp27_AST);
						match(GTEQ);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					addexpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop29;
				}
				
			} while (true);
			}
			relexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_13);
		}
		returnAST = relexpr_AST;
	}
	
	public final void addexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST addexpr_AST = null;
		
		try {      // for error handling
			multexpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop33:
			do {
				if ((LA(1)==PLUS||LA(1)==MINUS)) {
					{
					switch ( LA(1)) {
					case PLUS:
					{
						AST tmp28_AST = null;
						tmp28_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp28_AST);
						match(PLUS);
						break;
					}
					case MINUS:
					{
						AST tmp29_AST = null;
						tmp29_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp29_AST);
						match(MINUS);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					multexpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop33;
				}
				
			} while (true);
			}
			addexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_14);
		}
		returnAST = addexpr_AST;
	}
	
	public final void multexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST multexpr_AST = null;
		
		try {      // for error handling
			{
			primexpr();
			astFactory.addASTChild(currentAST, returnAST);
			}
			{
			_loop39:
			do {
				if (((LA(1) >= MULT && LA(1) <= MOD))) {
					{
					switch ( LA(1)) {
					case MULT:
					{
						AST tmp30_AST = null;
						tmp30_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp30_AST);
						match(MULT);
						break;
					}
					case DIV:
					{
						AST tmp31_AST = null;
						tmp31_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp31_AST);
						match(DIV);
						break;
					}
					case MOD:
					{
						AST tmp32_AST = null;
						tmp32_AST = astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp32_AST);
						match(MOD);
						break;
					}
					default:
					{
						throw new NoViableAltException(LT(1), getFilename());
					}
					}
					}
					{
					primexpr();
					astFactory.addASTChild(currentAST, returnAST);
					}
				}
				else {
					break _loop39;
				}
				
			} while (true);
			}
			multexpr_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_15);
		}
		returnAST = multexpr_AST;
	}
	
	public final void primexpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST primexpr_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case NUMBER:
			case IDENTIFIER:
			{
				t1();
				astFactory.addASTChild(currentAST, returnAST);
				primexpr_AST = (AST)currentAST.root;
				break;
			}
			case LPAREN:
			{
				match(LPAREN);
				expression();
				astFactory.addASTChild(currentAST, returnAST);
				match(RPAREN);
				primexpr_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_16);
		}
		returnAST = primexpr_AST;
	}
	
	public final void t1() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST t1_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case IDENTIFIER:
			{
				variable();
				astFactory.addASTChild(currentAST, returnAST);
				t1_AST = (AST)currentAST.root;
				break;
			}
			case NUMBER:
			{
				AST tmp35_AST = null;
				tmp35_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp35_AST);
				match(NUMBER);
				t1_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_16);
		}
		returnAST = t1_AST;
	}
	
	public final void boolop() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST boolop_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case OR:
			{
				AST tmp36_AST = null;
				tmp36_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp36_AST);
				match(OR);
				boolop_AST = (AST)currentAST.root;
				break;
			}
			case AND:
			{
				AST tmp37_AST = null;
				tmp37_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp37_AST);
				match(AND);
				boolop_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
		returnAST = boolop_AST;
	}
	
	public final void eqop() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST eqop_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case EQ:
			{
				AST tmp38_AST = null;
				tmp38_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp38_AST);
				match(EQ);
				eqop_AST = (AST)currentAST.root;
				break;
			}
			case NOTEQ:
			{
				AST tmp39_AST = null;
				tmp39_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp39_AST);
				match(NOTEQ);
				eqop_AST = (AST)currentAST.root;
				break;
			}
			case LT:
			{
				AST tmp40_AST = null;
				tmp40_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp40_AST);
				match(LT);
				eqop_AST = (AST)currentAST.root;
				break;
			}
			case LTEQ:
			{
				AST tmp41_AST = null;
				tmp41_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp41_AST);
				match(LTEQ);
				eqop_AST = (AST)currentAST.root;
				break;
			}
			case GT:
			{
				AST tmp42_AST = null;
				tmp42_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp42_AST);
				match(GT);
				eqop_AST = (AST)currentAST.root;
				break;
			}
			case GTEQ:
			{
				AST tmp43_AST = null;
				tmp43_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp43_AST);
				match(GTEQ);
				eqop_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
		returnAST = eqop_AST;
	}
	
	public final void addop() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST addop_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case MINUS:
			{
				AST tmp44_AST = null;
				tmp44_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp44_AST);
				match(MINUS);
				addop_AST = (AST)currentAST.root;
				break;
			}
			case PLUS:
			{
				AST tmp45_AST = null;
				tmp45_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp45_AST);
				match(PLUS);
				addop_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
		returnAST = addop_AST;
	}
	
	public final void multop() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST multop_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case MULT:
			{
				AST tmp46_AST = null;
				tmp46_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp46_AST);
				match(MULT);
				multop_AST = (AST)currentAST.root;
				break;
			}
			case DIV:
			{
				AST tmp47_AST = null;
				tmp47_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp47_AST);
				match(DIV);
				multop_AST = (AST)currentAST.root;
				break;
			}
			case MOD:
			{
				AST tmp48_AST = null;
				tmp48_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp48_AST);
				match(MOD);
				multop_AST = (AST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
		returnAST = multop_AST;
	}
	
	public final void option() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST option_AST = null;
		
		try {      // for error handling
			match(DOUBLECOLON);
			guard();
			astFactory.addASTChild(currentAST, returnAST);
			terminator();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop56:
			do {
				if ((_tokenSet_8.member(LA(1)))) {
					statement();
					astFactory.addASTChild(currentAST, returnAST);
					terminator();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop56;
				}
				
			} while (true);
			}
			option_AST = (AST)currentAST.root;
			
			option_AST = (AST)astFactory.make( (new ASTArray(2)).add(astFactory.create(OPTION,"option")).add(option_AST));
			
			currentAST.root = option_AST;
			currentAST.child = option_AST!=null &&option_AST.getFirstChild()!=null ?
				option_AST.getFirstChild() : option_AST;
			currentAST.advanceChildToEnd();
			option_AST = (AST)currentAST.root;
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_17);
		}
		returnAST = option_AST;
	}
	
	public final void guard() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		AST guard_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LITERAL_assert:
			{
				assertion();
				astFactory.addASTChild(currentAST, returnAST);
				guard_AST = (AST)currentAST.root;
				break;
			}
			case LITERAL_break:
			{
				AST tmp50_AST = null;
				tmp50_AST = astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp50_AST);
				match(LITERAL_break);
				guard_AST = (AST)currentAST.root;
				break;
			}
			default:
				if ((LA(1)==LPAREN||LA(1)==NUMBER||LA(1)==IDENTIFIER) && (_tokenSet_9.member(LA(2)))) {
					expression();
					astFactory.addASTChild(currentAST, returnAST);
					guard_AST = (AST)currentAST.root;
				}
				else if ((LA(1)==IDENTIFIER) && (LA(2)==ASSIGN)) {
					assignment();
					astFactory.addASTChild(currentAST, returnAST);
					guard_AST = (AST)currentAST.root;
				}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
		returnAST = guard_AST;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"OPTION",
		"EXPRESSION",
		"ASSIGNMENT",
		"DECLARATION",
		"PROCESS",
		"OREXPR",
		"ANDEXPR",
		"EQEXPR",
		"RELEXPR",
		"ADDEXPR",
		"MULTEXPR",
		"\"active\"",
		"\"proctype\"",
		"LPAREN",
		"RPAREN",
		"LBRACE",
		"RBRACE",
		"\"break\"",
		"OR",
		"AND",
		"EQ",
		"NOTEQ",
		"LT",
		"LTEQ",
		"GT",
		"GTEQ",
		"PLUS",
		"MINUS",
		"MULT",
		"DIV",
		"MOD",
		"NUMBER",
		"ASSIGN",
		"\"assert\"",
		"\"if\"",
		"\"fi\"",
		"\"do\"",
		"\"od\"",
		"DOUBLECOLON",
		"\"short\"",
		"IDENTIFIER",
		"SEMICOLON",
		"ARROW",
		"DIGIT",
		"LETTER",
		"NEWLINE",
		"WS",
		"SL_COMMENT",
		"ML_COMMENT"
	};
	
	protected void buildTokenTypeASTClassMap() {
		tokenTypeToASTClassMap=null;
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { 2L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = { 19138376531968L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = { 105553116266496L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = { 35081296183296L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = { 17592186044416L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	private static final long[] mk_tokenSet_5() {
		long[] data = { 105656191680512L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_5 = new BitSet(mk_tokenSet_5());
	private static final long[] mk_tokenSet_6() {
		long[] data = { 32770L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_6 = new BitSet(mk_tokenSet_6());
	private static final long[] mk_tokenSet_7() {
		long[] data = { 1048576L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_7 = new BitSet(mk_tokenSet_7());
	private static final long[] mk_tokenSet_8() {
		long[] data = { 19138376499200L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_8 = new BitSet(mk_tokenSet_8());
	private static final long[] mk_tokenSet_9() {
		long[] data = { 123214017724416L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_9 = new BitSet(mk_tokenSet_9());
	private static final long[] mk_tokenSet_10() {
		long[] data = { 105553116528640L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_10 = new BitSet(mk_tokenSet_10());
	private static final long[] mk_tokenSet_11() {
		long[] data = { 105553120722944L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_11 = new BitSet(mk_tokenSet_11());
	private static final long[] mk_tokenSet_12() {
		long[] data = { 105553129111552L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_12 = new BitSet(mk_tokenSet_12());
	private static final long[] mk_tokenSet_13() {
		long[] data = { 105553179443200L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_13 = new BitSet(mk_tokenSet_13());
	private static final long[] mk_tokenSet_14() {
		long[] data = { 105554186076160L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_14 = new BitSet(mk_tokenSet_14());
	private static final long[] mk_tokenSet_15() {
		long[] data = { 105557407301632L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_15 = new BitSet(mk_tokenSet_15());
	private static final long[] mk_tokenSet_16() {
		long[] data = { 105587472072704L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_16 = new BitSet(mk_tokenSet_16());
	private static final long[] mk_tokenSet_17() {
		long[] data = { 7146825580544L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_17 = new BitSet(mk_tokenSet_17());
	
	}
