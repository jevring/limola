package compiled;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-jun-08 : 00:18:48
 */
public class CompiledAssertion extends CompiledStatement {
    public CompiledAssertion(short[] state) {
        super(state);
    }

    public short execute() {
        return 1;//$$statement$$ //something like 8 >> state[14];, where state[14] would have to be the replacement string for the variable name
    }
}
