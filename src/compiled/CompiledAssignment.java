package compiled;

import java.lang.reflect.Array;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-jun-08 : 00:15:40
 */
public class CompiledAssignment extends CompiledStatement {
    public CompiledAssignment(short[] state) {
        super(state);
    }

    public short execute() {
        state[2] = (short) (8 * state[12]); // replace a $$statemtn$$ string
        return 1;
    }
}
