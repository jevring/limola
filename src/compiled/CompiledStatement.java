package compiled;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-jun-08 : 00:19:04
 */
public class CompiledStatement {
    protected short[] state;

    public CompiledStatement(short[] state) {
        System.arraycopy(state, 0, this.state, 0, state.length);
    }

    public short[] getState() {
        return state;
    }
}
