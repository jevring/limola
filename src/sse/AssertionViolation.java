package sse;

import ast.Statement;
import ast.Assertion;

/**
 * Represents an assertion violation. has a depth, a state, and a link to the assertion that was violated.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @version 4-jun-2007 : 10:24:40
 */
public class AssertionViolation {
    private int depth;
    private State state;
    private Assertion assertion;


    public AssertionViolation(State state, Assertion assertion) {
        this.depth = state.getDepth();
        this.state = state;
        this.assertion = assertion;
    }

    public int getDepth() {
        return depth;
    }

    public State getState() {
        return state;
    }

    public Assertion getAssertion() {
        return assertion;
    }
}
