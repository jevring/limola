package sse;

import ast.Assertion;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-28 : 18:20:33
 */
public class AssertionFailedException extends Throwable {
    private Assertion assertion;
    private State state;
    private AssertionViolation assertionViolation;

    private AssertionFailedException(Assertion assertion, State state) {
        super("Assertion violated: " + assertion.getDescription());
        this.assertion = assertion;
        this.state = state;
    }

    public AssertionFailedException(AssertionViolation assertionViolation) {
        this(assertionViolation.getAssertion(), assertionViolation.getState());
        this.assertionViolation = assertionViolation;
    }

    public Assertion getAssertion() {
        return assertion;
    }

    public State getState() {
        return state;
    }

    public AssertionViolation getAssertionViolation() {
        return assertionViolation;
    }
}
