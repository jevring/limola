package sse;

import ast.*;
import ast.Process;
import parser.UnsupportedOperatorException;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Represents a state in the execution. Contains an array containing the program counters for the different processes (pids)
 * and the variable values for the variables (both global and local to each process).
 * Also contains some static references to useful data.
 */
public class State {
	private static Model model;
    private static StateSpaceExplorer stateSpaceExplorer;
    private static int stateSize;
    private static int numberOfProcesses = 0;
    private short[] stateRepresentation;
    private State previousState;
    private int depth = 1;

    /**
     *
     * the state needs to have access to the model to determine what the possible next statements are
	 * the state needs to know WHERE in the model we are, and what the possible next Statement:s are, so that we
	 * can generate new states for the possible enabled statements
     *
	 * each pid represents an active proctype. if the process is finished, the pc for that process is -1
	 * this requires that we know:
	 * - the number of processes
	 * - the number of global variables
	 * - the number of local variables for each process
	 * [pid1.pc, pid2.pc, pid3.pc, globvar1, globvar2, pid1.var1, pid1.var2, pid2.var1, pid3.var1, pid3.var2]
     *
     * @param state the state to base this state on. This provided state will be copied.
     */
    public State(State state) {
        // make a copy of the state we passed as a parameter (basically copy the state vector representation)
        //this(state.getModel()); // this merely references the model, and creates the state representation array
        stateRepresentation = new short[stateSize];
        System.arraycopy(state.getStateRepresentation(), 0, stateRepresentation, 0, stateSize);
        this.previousState = state; // this is true because this is a copy that is going to get modified to hold the new state
        depth = state.getDepth() + 1;
        //System.out.println("OLD: " + state + "; NEW: " + this);
    }

    // determining the size of the state vector: http://moinmoin.riters.com/JINX/index.cgi/Size_20of_20an_20Object
    // we should ideally just have an array as a state vector (on the word boundry. that would be 32 for most x86 systems, and probably 64 for your mac, if it is new enough ;))
    // we'd then use a static translator object to reference the values
    // this is SUCH bad OO practise, but it's what we do when we are hunting bytes.

    public State(Model m, StateSpaceExplorer sse) {
		model = m; // first state created
        stateSpaceExplorer = sse;
        // we are putting the pids first. even though that information is available LAST when traversing the AST, it still seems more logical.
        // when we get the variable values, we offset them with the number of processes. (just like we have described above)
        numberOfProcesses = model.getNumberOfProcesses();
        stateSize = numberOfProcesses + model.getNumberOfVariables();
        stateRepresentation = new short[stateSize];
    }

    /**
     * Checks the state and determine what possible paths could be taken from the state we are currently in.
     * It tries to execute those statements, and returns the modified states from the statements that were executable.
     *
     * @return a list of states reachable from the current state.
     * @throws UnsupportedOperatorException
     * @throws AssertionFailedException
     */
    public ArrayList<State> getSuccessors() throws UnsupportedOperatorException, AssertionFailedException {
		// create a set of states that represent the next state in the path, if the respective enabled paths were taken
        ArrayList<State> successors = new ArrayList<State>();

        // at any point, the next available statments for a state are the following:
        // - a normal step in ANY of the processes in the model
        // - all the (executable) options in a branching statement (do, if) in ANY process.

        // for each process that is alive (not at end, not at deadlock or at a static deadlock (when a static deadlock is found, throw an exception))
        // add the next statement from that process to the list of successors
        // if that statement happens to be a do or if, add all the options
        // (special case for break, but a break already has a 'next' statement nicely lined up)

        // for each possible statement we find, we check them all to see if they are executable.
        // the executable ones make it in to the list that we finally return to the sse

        ArrayList<Statement> possibleSuccessors = new ArrayList<Statement>();

        ArrayList<Process> processes = model.getProcesses();
//        System.out.print("Possible: [");
        for (Process p : processes) {
            // check if the process is alive
            // get the pc for this process out of the state and see if the statement represented by that pid is the [end] statement
            int pc = getPcForPid(p.getPid()); // this will tell us where the pc is in this process (in the current state)
            //Statement statement = p.getStatement(pc); // get the statement that corresponds to this pc
            // since the state sets the pc to the statement that got executed, we have to advance the pc here.
            Statement statement = p.getStatement(pc).getNextStatement(); // get the statement that corresponds to the next pc
            if (!statement.isEndStatement()) {
                // this is an acceptable statement. now check if it is a branching statement
                if (statement instanceof BranchingStatement) {
                    // because we do this, we might end up with a depth that is much lower than expected, because we don't count the loop back as a statement
                    BranchingStatement brs = (BranchingStatement)statement;
                    // a branching statement, add the options to the list of possible successors
                    for (Option option : brs.getOptions()) {
//                        System.out.print(option.getGuard().getDescription() + "] [");
                        possibleSuccessors.add(option.getGuard());
                    }
                } else {
                    // even if we are in the middle of an option, we will end up here, since it is a linear execution, and other options in that branchign statment can't be taken until we are finished with this one
                    // if it is not a branching statement, just add the statement to the list of possible statements
//                    System.out.print(statement.getDescription() + "] [");
                    possibleSuccessors.add(statement);
                }
            }
        }



//        System.out.println("] ");
//        System.out.print("Executable: [");
        for (Statement statement : possibleSuccessors) {
            State stateCopy = new State(this);
            ExecutableStatement ex = new ExecutableStatement(stateCopy, statement);
            try {
                boolean isExecutable = ex.isExecutable();
                if (isExecutable) {
//                    System.out.print(statement.getDescription() + "(" + statement.getPid() + ":" + statement.getPc() + ")] [");
                    successors.add(ex.getExecutedState());
                }
            } catch (AssertionFailedException e) {
                stateSpaceExplorer.addAssertionViolation(e.getAssertionViolation());
                if (StateSpaceExplorer.isStopAtFirstError()) {
                    throw e;
                }
            }
		}
//        System.out.println("] From: " + this );
        return successors;
	}

    /**
     * @deprecated
     * @see #getSuccessors() 
     * @return
     * @throws UnsupportedOperatorException
     * @throws AssertionFailedException
     */
    public ArrayList<State> getSuccessorsPossiblyBroken() throws UnsupportedOperatorException, AssertionFailedException {
		// create a set of states that represent the next state in the path, if the respective enabled paths were taken
        ArrayList<State> successors = new ArrayList<State>();

        // at any point, the next available statments for a state are the following:
        // - a normal step in ANY of the processes in the model
        // - all the (executable) options in a branching statement (do, if) in ANY process.

        // for each process that is alive (not at end, not at deadlock or at a static deadlock (when a static deadlock is found, throw an exception))
        // add the next statement from that process to the list of successors
        // if that statement happens to be a do or if, add all the options
        // (special case for break, but a break already has a 'next' statement nicely lined up)

        // for each possible statement we find, we check them all to see if they are executable.
        // the executable ones make it in to the list that we finally return to the sse

//        ArrayList<Statement> possibleSuccessors = new ArrayList<Statement>();

//        System.out.print("adding statements to possible: [");
        for (Process p : model.getProcesses()) {
            // check if the process is alive
            // get the pc for this process out of the state and see if the statement represented by that pid is the [end] statement
            int pc = getPcForPid(p.getPid()); // this will tell us where the pc is in this process (in the current state)
            Statement statement = p.getStatement(pc); // get the statement that corresponds to this pc
            ExecutableStatement ex;
            State stateCopy = new State(this);
            if (!statement.isEndStatement()) {
                // this is an acceptable statement. now check if it is a branching statement
                if (statement instanceof BranchingStatement) {
                    BranchingStatement brs = (BranchingStatement)statement;
                    // a branching statement, add the options to the list of possible successors
                    for (Option option : brs.getOptions()) {
//                        System.out.print(option.getGuard().getDescription() + "] [");
                        ex = new ExecutableStatement(stateCopy, option.getGuard());
                        boolean isExecutable = false; // this is where the assertion violation comes out
                        try {
                            isExecutable = ex.isExecutable();
                            if (isExecutable) {
                                successors.add(ex.getExecutedState());
                            }
                        } catch (AssertionFailedException e) {
                            stateSpaceExplorer.addAssertionViolation(e.getAssertionViolation());
                            if (StateSpaceExplorer.isStopAtFirstError()) {
                                throw e;
                            }
                        }

                    }
                } else {
                    // even if we are in the middle of an option, we will end up here, since it is a linear execution, and other options in that branchign statment can't be taken until we are finished with this one
                    // if it is not a branching statement, just add the statement to the list of possible statements
//                    System.out.print(statement.getDescription() + "] [");
                    ex = new ExecutableStatement(stateCopy, statement);
                    try {
                        boolean isExecutable = ex.isExecutable();
                        if (isExecutable) {
                            successors.add(ex.getExecutedState());
                        }
                    } catch (AssertionFailedException e) {
                        stateSpaceExplorer.addAssertionViolation(e.getAssertionViolation());
                        if (StateSpaceExplorer.isStopAtFirstError()) {
                            throw e;
                        }
                    }
                }
            }
        }
        //System.out.println("We have " + possibleSuccessors.size() + " possible states");
        

/*
//        System.out.print("] Actally executable: [");
        for (Statement statement : possibleSuccessors) {
            // note: this needs to send in a copy of this state
            ExecutableStatement sec = new ExecutableStatement(new State(this), statement);
            if (sec.isExecutable()) { // _todo: when we throw assertion violations out, we should save them all to find the shortest
                // note: this doesn't actually getExecutedState the statement, it has already been executed when were were checking if it was possible or not
                // this way we save on execution time
//                System.out.print(statement.getDescription() + "] [");
                successors.add(sec.getExecutedState());
				// note: we have the pc embedded into this object to prevent having to do multiple pid + pc lookups in the model to get a statement.
			}
		}
//        System.out.println("]");
*/
        return successors;
	}

    /**
     * Returns the program counter (pc) in the current state for the specified pid.
     * @param pid
     * @return
     */
    private short getPcForPid(int pid) {
        return stateRepresentation[pid];
    }

    public void setPCForPid(short pc, int pid) {
        stateRepresentation[pid] = pc;
    }

    /**
     * Sets the value of a variable in the state representation.
     * @param variableIndex the index where to find the variable.
     * @param value the value of the variable.
     */
    public void setVariable(int variableIndex, short value) {
        //System.out.println("Setting variable at array index " + (numberOfProcesses + variableIndex) + " to " + value);
        stateRepresentation[numberOfProcesses + variableIndex] = value;
    }

    public short getValueByVariableIndex(int variableIndex) {
        //System.out.println("returning the value of variable at array index " + (numberOfProcesses + variableIndex) + " (" + stateRepresentation[numberOfProcesses + variableIndex] + ")");
        return stateRepresentation[numberOfProcesses + variableIndex];
    }

    public short[] getStateRepresentation() {
        return stateRepresentation;
    }

    /**
     * Checks if all processes are in the [end] state or not.
     * This is used when we reach the end of an execution branch. If the end is reached and this method returns false,
     * we have a deadlock.
     * @return true if all processes have ended, false otherwise.
     */
    public boolean allProcessesHaveEnded() {
        // this checks the statements represented by the PCs in the stateRepresentation to see if they are end statements of not
        for (int i = 0; i < numberOfProcesses; i++) {
            Statement s = model.getProcess(i).getStatement(stateRepresentation[i]);
            if (!s.isEndStatement()) {
                return false;
            }
        }
        return true;
    }


    public State getPreviousState() {
        return previousState;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;

        return Arrays.equals(stateRepresentation, state.stateRepresentation);

    }

    /**
     * Used for indexing the HashSet that contains our states. Even though the hash function itself is only capable of uniquely
     * addressing states of ~32000 byte, the HashSet contains a LinkedList implementation that makes sure that states are not lost.
     * Had we implemented bitstate hashing, we would still have used this as our primary hash function, and we would then have implemented
     * some extra offset to bring the improper collisions to a mimimum.
     * @return
     */
    public int hashCode() {
        int i = Arrays.hashCode(stateRepresentation);
        //System.err.println("Hashed state " + this + " to code: " + i);
        return i; 
    }


    /**
     * String representation of the state.
     * @return
     */
    public String toString() {
        String s = "[State@{";
        s += stateRepresentation[0];
        for (int i = 1; i < stateRepresentation.length; i++) {
            short i1 = stateRepresentation[i];
            s += ", " + i1;
        }
        return s + "}]";
    }

    /**
     * Returns the depth at which this state can be found.
     * 
     * @return
     */
    public int getDepth() {
        return depth;
    }
}
