package sse;

import ast.*;
import parser.LimolaTokenTypes;
import parser.UnsupportedOperatorException;

/**
 * this is a representation of the statement from the AST, but since this needs to know about the state that we
 * are currently in, to determine if it is executable or not, we have another representation here, to avoid high coupling
 */
public class ExecutableStatement {
    private State state;
    private Statement statement;
    private boolean pcset = false;

    /**
     * Mechanism for checking is a state is executable or not.
     * During this execution, the supplied state can be changed, so therefore we must pass a copy of the state to this class.
     * @param state a copy of the state for which we want to see if a statement is executable or not.
     * @param statement the statment we want to know the executability of.
     */
    public ExecutableStatement(State state, Statement statement) {
        this.state = state;
        this.statement = statement;
    }

    // if we make the model lookup fast enough, we could accept a pid + pc pair here, instead of a statement
    /**
     * Tries to execute the statement (and updates a copy of the state while doing so).
     * @return true if the staement is executable, false otherwise.
     * @throws UnsupportedOperatorException
     * @throws AssertionFailedException thrown if an assertion was found to not hold.
     */
    public boolean isExecutable() throws UnsupportedOperatorException, AssertionFailedException {
        //System.out.println("checking statement " + statement.getDescription() + " for executability in state " + state);
        // based on what the currentState looks like, we can examine this
		// statement and determine if it is executable or not
        // take the expression, extract the variables from the state (copy), and getExecutedState the expression. return true if the expression is <0
        //long a = System.nanoTime();
        boolean isExecutable = evaluate(statement) > 0;
        //long b = System.nanoTime();
        //System.out.println("evaluate took " + (b - a) + " nanoseconds to execute " + statement.getDescription());
        if (isExecutable && !pcset) { // && !Break statement (because then we'll update the PC ourselves)
            // update the program counter
            //state.setPCForPid((short) statement.getNextStatement().getPc(), statement.getPid());
            state.setPCForPid((short) statement.getPc(), statement.getPid()); // setting the pc to the statement that JUST got executed.
        }
        return isExecutable;
    }

    /**
     * Takes care of actually evaluating the statement represented by this object.
     * Depending on the type of statement, different checks are run. This method can recursively invoke itself to dig deeper into the statement.
     *
     * @param statement the statement to evaluate.
     * @return the result of the statement, Short.MIN_VALUE <= return <= Short.MAX_VALUE. (1 or 0 for boolean expressions)
     * @throws UnsupportedOperatorException
     * @throws AssertionFailedException
     */
    private short evaluate(Statement statement) throws UnsupportedOperatorException, AssertionFailedException {
        if (statement instanceof SimpleExpression) {
            // this is a simple statement that we can get the value of.
            SimpleExpression ss = (SimpleExpression)statement;
            if (ss.isVariable()) {
                return state.getValueByVariableIndex(ss.getVariableIndex());
            } else {
                // return the value of the statement
                return ss.getValue();
            }
        } else if (statement instanceof Assignment) {
            Assignment as = (Assignment)statement;
            // assignments go right to left only
            short value = evaluate(as.getValue());
            //System.out.println("evaluated " + statement.getDescription() + " to be " + value);

            state.setVariable(as.getVariableIndex(), value);
            return 1;
        } else if(statement instanceof Assertion) {
            // note: doing is this way would make statements with nested assertions valid.
            Assertion as = (Assertion)statement;
            short ce = evaluate(as.getContainingExpression());
            //System.out.println("evaluated " + statement.getDescription() + " to be " + ce);
            if(ce < 1) {
                //System.out.println("throwing assertion at depth " + state.getDepth());
                throw new AssertionFailedException(new AssertionViolation(state, as));
            } else {
                return 1;
            }
        } else if(statement instanceof Break) {
            // updating the PC to the END of the loop that this break belongs to, don't change any variables, return
            //state.setPCForPid((short) statement.getNextStatement().getPc(), statement.getPid());
            state.setPCForPid((short) statement.getPc(), statement.getPid());
            pcset = true;
            return 1;
        } else if (statement instanceof CompoundExpression) {
            return evaluateCompountExpression((CompoundExpression)statement);
        } else {
            // This catches the ONLY start statement, which is always executable 
            return 1;
        }
    }

    /**
     * Helper function to evaluate compound methematical expressions. If the compunt expression is a boolean expression, it
     * passes the buck along.
     * @param stat
     * @return
     * @throws UnsupportedOperatorException
     * @throws AssertionFailedException
     */
    private short evaluateCompountExpression(CompoundExpression stat) throws UnsupportedOperatorException, AssertionFailedException {
        switch (stat.getOperator()) {
            case LimolaTokenTypes.PLUS:     return (short)(evaluate(stat.getLh()) + evaluate(stat.getRh()));
            case LimolaTokenTypes.MINUS:    return (short)(evaluate(stat.getLh()) - evaluate(stat.getRh()));
            case LimolaTokenTypes.DIV:      return (short)(evaluate(stat.getLh()) / evaluate(stat.getRh()));
            case LimolaTokenTypes.MULT:     return (short)(evaluate(stat.getLh()) * evaluate(stat.getRh()));
            case LimolaTokenTypes.MOD:      return (short)(evaluate(stat.getLh()) % evaluate(stat.getRh()));
            default:                        return (short)(evaluateBooleanExpression(stat) ? 1 : 0);
        }
    }

    /**
     * Evaluates boolean expression.
     * @param statement
     * @return
     * @throws UnsupportedOperatorException
     * @throws AssertionFailedException
     */
    private boolean evaluateBooleanExpression(CompoundExpression statement) throws UnsupportedOperatorException, AssertionFailedException {

        switch (statement.getOperator()) {
            case LimolaTokenTypes.AND:      return (evaluate(statement.getLh()) >= 1 && evaluate(statement.getRh()) >= 1);
            case LimolaTokenTypes.OR:       return (evaluate(statement.getLh()) >= 1 || evaluate(statement.getRh()) >= 1);
            case LimolaTokenTypes.GT:       return (evaluate(statement.getLh()) > evaluate(statement.getRh()));
            case LimolaTokenTypes.GTEQ:     return (evaluate(statement.getLh()) >= evaluate(statement.getRh()));
            case LimolaTokenTypes.LT:       return (evaluate(statement.getLh()) < evaluate(statement.getRh()));
            case LimolaTokenTypes.LTEQ:     return (evaluate(statement.getLh()) <= evaluate(statement.getRh()));
            case LimolaTokenTypes.EQ:       return (evaluate(statement.getLh()) == evaluate(statement.getRh()));
            case LimolaTokenTypes.NOTEQ:    return (evaluate(statement.getLh()) != evaluate(statement.getRh()));
            default:                        throw new UnsupportedOperatorException(statement.getOperator());
        }
    }

    /**
     * Returns the state that the execution generated. This will contain the updated process counters and variable values
     * that this statement resulted in.
     * @return
     */
    public State getExecutedState() {
        return state;
    }

}
