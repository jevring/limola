package sse;

/**
 * Represents a deadlock in the code. Has a depth and a state.
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-jun-03 : 17:38:20
 */
public class Deadlock {
    private long depth;
    private State state;

    public Deadlock(long depth, State state) {
        this.depth = depth;
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public long getDepth() {
        return depth;
    }
}
