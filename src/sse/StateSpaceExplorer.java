package sse;

import ast.Model;
import ast.Statement;
import parser.AstToObjectEngine;
import parser.UnsupportedOperatorException;

import java.io.*;
import java.util.*;

/**
 * <pre>
 * The actual modelchecker. It has two algorithms for state space exploration.
 * One is a purely recursive depth first search. (See #dfs(State))
 * The other is an emulated depth first search that uses a stack to emulate the recursive calls. (See #dfsSafe(State)).
 * Usually the performance of the two are on par, but in some cases, one will win out over the other.
 * Our record run of 292k states per second was done by the emulated dfs.
 * Using the emulated dfs also gets rid of the problem of running out of stack space (which is fixable by the java switch -XssnM
 * used to set the available stack space for the JVM.
 *
 * The model checker takes some arguments (settable via command line arguments):
 * java StateSpaceExplorer filename [verbosity=n] [maxdepth=n] [stop_at_first_error] [no_recursion]
 *
 * <code>filename</code> is the name of the file containing the limola model.
 * <code>verbosity</code> ranges from 0 to 4, with 4 being the highest. This controls how much output the user sees.
 * <code>maxdepth</code> indicates how deep we want the depth first search algorithm to go.
 * In the case of the emulated version, this is the height of the stack.
 * <code>stop_at_first_error</code> using this flag tells the state space exlorer to stop searching for errors
 * after the first error has been found.
 * <code>no_recursion</code> tells the state space explorer to use the emulated depth first search algorithm.
 *
 *  verbosity = 0 : normal
 *  verbosity = 1 : extra state information + print all errors
 *  verbosity = 2 : 1 + print trail to STDOUT
 *  verbosity = 3 : 2 + statement information in the trail
 *  verbosity = 4 : 3 + state information in the trail.
 * </pre>
 */
public class StateSpaceExplorer {
    // this is implemented by the java API as a HashMap, with dummy objects as the value in the map, and the entry as the key.
    // this will NOT cause the problem that bitstate hashing has, as the underlying HashMap is implemented as an array with a linked list
    // if the index that the hash value points to happens to be the same as some other, already added, value, but where the actual values are different.
    private HashSet<State> stateSpace;
    private HashSet<Integer> bitstateStateSpace;
    private TreeMap<Integer, Deadlock> deadlocks;
    private TreeMap<Integer, AssertionViolation> assertionViolations;
    //private ArrayDeque<State> stack;
    private LinkedList<State> stack;
    private PrintWriter trail;
    private long cacheHits = 0;
    private static Model model;
    private static String filename;
    private static long maxdepth = 10000;
    private static int verbosity = 0;
    private static boolean stopAtFirstError = false;
    private static boolean noRecursion = false;
    private int biggestDepth = 0;

    /*
    Best run for dfsSafe is on mutex-5.lila: (almost 300k states/s, but the biggest "depth" is much higher)(in a P4-3.2Ghz with 1GB ram)
    Run complete.
    3145858 states seen.
    10.745 seconds taken.
    292774.12750116334 states per second.
    62917160 bytes statespace.
    20 bytes state vector.
    Biggest depth 589871
    5407145 hits on items that were already in the statespace.
    The following 5 assertion violation(s) were found
    error: assertion violation
    AssertionViolation@{pid=0:[depth=196621;pc=3;statement=:: assert(mutex != 2)]}
    AssertionViolation@{pid=0:[depth=196627;pc=3;statement=:: assert(mutex != 2)]}
    ...

    */
    /**
     * This is the emulated depth first search algorithm. It uses a stack (LIFO) structure to emulate what the recursive
     * call stack would have looked like. This will visit the states in a different order than the recursive depth first
     * search, as it will generate all successor states for one branch (though not for a complete level, so it is not a breadth first search.)
     * before diving in to the next depth.
     * For each iteration of the loop, we push the sucessors on the stack, then we loop and treat the top of the stack
     * (which is the last successor added) in the same manner.
     *
     * @param state the state to expand on. Since this is not recursive, this will be the start state for the model.
     * @throws AssertionFailedException
     * @throws UnsupportedOperatorException
     * @throws MaxDepthReachedException
     */
    public void dfsSafe(State state) throws AssertionFailedException, UnsupportedOperatorException, MaxDepthReachedException {
        stack.push(state);
        while (stack.size() != 0) {
            // the problem with this method is that if you have complex models and one or more infinite loops in them, it takes AGES for this to stop, since it generates all siblings to a tree node before diving into the next level.
            state = stack.pop();
            int depth = state.getDepth();
            // removing the state space check gets us from 112k to 116k states per second
            if (depth >= maxdepth) throw new MaxDepthReachedException(depth);
            if (depth > biggestDepth) biggestDepth = depth;
        
            // since the hash code generated for this array results in an int, we have roughly 4-5 billion states to fill before we run in to any bitstate hashing problems.
            stateSpace.add(state);
            //fullStates.put(state.hashCode(), state); // this should cause our memory usage to spiral out of control
            ArrayList<State> successors = state.getSuccessors();
            if (successors.size() == 0) {
                // if we have no successors, and there are processes which are still not in their end state (or in promela, a 'progress' state) then we have a deadlock
                if (!state.allProcessesHaveEnded()) {
//                    System.err.println("Deadlock found at depth " + depth + " : " + state);
                    deadlocks.put(depth, new Deadlock(depth, state));
//                    visualizeDeadlock(state);
                    if (stopAtFirstError) {
                        return;
                    }
                } // else we just found some tree that ended successfully (which we will do a lot)
            } else {
                for(State successor : successors) { // get enabled transitions
                    if (!stateSpace.contains(successor)) {
                        stack.push(successor);
                    } else {
                        cacheHits++;
                    }
                    // if there are no successors, and we don't have all the PCs in the [end] state, we have a deadlock. (note: this must be checked for ALL successors (all pids)
                }
            }
        }
    }

    /**
     * This uses a recursive algorithm to check the model. use the <code>no_recursion</code> command line parameter to use the emulated version of this method of checking.
     * This can run out of stack space, so use -Xss2048k to set the stackspace in tje JVM to a higher number.
     * -Xss2048k lets you do 10000 with no problem.
     * @see #dfsSafe(State)
     * @param state the state to examine.
     * @throws UnsupportedOperatorException
     * @throws AssertionFailedException
     * @throws MaxDepthReachedException
     * @throws DeadlockFoundException
     */
    public void dfs(State state) throws UnsupportedOperatorException, AssertionFailedException, MaxDepthReachedException, DeadlockFoundException {
        int depth = state.getDepth();
        if (depth > biggestDepth) biggestDepth = depth;
        if (depth >= maxdepth) throw new MaxDepthReachedException(depth);
//        System.out.println("Adding state to seen: " + state);

        stateSpace.add(state);
        ArrayList<State> successors = state.getSuccessors();
        if (successors.size() == 0) {
            // if we have no successors, and there are processes which are still not in their end state (or in promela, a 'progress' state) then we have a deadlock
            if (!state.allProcessesHaveEnded()) {
//                System.err.println("Deadlock found at depth " + depth + " : " + state);
                Deadlock deadlock = new Deadlock(depth, state);
                deadlocks.put(depth, deadlock);
                if (stopAtFirstError) {
                    throw new DeadlockFoundException(deadlock);
                }
            } // else we just found some tree that ended successfully (which we will do a lot)
        } else {
            for(State successor : successors) { // get enabled transitions
                if (!stateSpace.contains(successor)) {
                    dfs(successor);
                } else {
                    cacheHits++;
                }
                // if there are no successors, and we don't have all the PCs in the [end] state, we have a deadlock. (note: this must be checked for ALL successors (all pids)
            }
        }
    }

    public StateSpaceExplorer(Model m) throws FileNotFoundException {
        model = m;
        deadlocks = new TreeMap<Integer, Deadlock>();
        assertionViolations = new TreeMap<Integer, AssertionViolation>();
        //stack = new ArrayDeque<State>(); // pass a size to this initially, to allocate large and avoid having to resize?
        stack = new LinkedList<State>();
        trail = new PrintWriter(new FileOutputStream(filename + ".trail"));
    }

    /**
     * Starts the model checker. This method is also responsible for giving statistics (depending on verbosity level) at
     * the end, and catch any exceptions that are thrown during explroation.
     */
    public void start() {
        State startState = new State(model, this);
        // setting this here doesn't really do anything for small problems, but for bigger problems it really gives us a boost
        stateSpace = new HashSet<State>(); // if we have big problems, we could pre/set this size here to make it go faster, but since we don't know in advance...
        long start = System.currentTimeMillis();
        try {
            if (noRecursion) {
                dfsSafe(startState); // initial state (all pcs at 0)
            } else{
                dfs(startState); // initial state (all pcs at 0)
            }
            System.out.println("Run complete.");

        } catch (UnsupportedOperatorException e) {
            System.err.println(e.getMessage());
        } catch (AssertionFailedException e) {
            // This will be visualized in the loop below.
            System.err.println(e.getMessage());
        } catch (MaxDepthReachedException e) {
            System.err.println(e.getMessage());
        } catch (DeadlockFoundException e) {
            // this will be handled by the printout below
            System.err.println(e.getMessage());
        }
        long end = System.currentTimeMillis();
        double seconds = (double)(end - start) / 1000.0D;
        int states = 0;
        states = stateSpace.size();
        System.out.println(states + " states seen.");
        System.out.println(seconds + " seconds taken.");
        System.out.println((states / seconds) + " states per second.");
        int stateVectorSize = ((startState.getStateRepresentation().length * 2) + 4 + 4 + 4); // this is state vector + int index + references to previous state + int depth
        System.out.println((states * stateVectorSize) + " bytes statespace.");
        System.out.println(stateVectorSize + " bytes state vector.");
        System.out.println("Biggest depth " + biggestDepth);
        if (verbosity >= 1) {
            System.out.println(cacheHits + " hits on items that were already in the statespace.");
        }
        if (assertionViolations.size() > 0) {
            System.err.println("error: assertion violation");
            System.out.println(assertionViolations.size() + " assertion violation(s) were found");
            if (verbosity < 1) System.out.println("Showing shortest");
            boolean pathVisualized = false;
            for (Map.Entry<Integer, AssertionViolation> entry : assertionViolations.entrySet()) {
                visualizeAssertionViolation(entry.getValue());
                // we always visualize the path. the visualizer determines if we should do it to disk or not
                if (!pathVisualized) {
                    visualizePath(entry.getValue().getState());
                    pathVisualized = true;
                }
                if (verbosity < 1) break;
            }
        } else {
            if (deadlocks.size() > 0) {
                System.err.println("error: deadlock");
                System.out.println(deadlocks.size() + " deadlock(s) were found");
                if (verbosity < 1) System.out.println("Showing shortest");
                boolean pathVisualized = false;
                for (Map.Entry<Integer, Deadlock> entry : deadlocks.entrySet()) {
                    visualizeDeadlock(entry.getValue());
                    // we always visualize the path. the visualizer determines if we should do it to disk or not
                    if (!pathVisualized) {
                        visualizePath(entry.getValue().getState());
                        pathVisualized = true;
                    }
                    if (verbosity < 1) break;
                }
            } else {
                System.out.println("no errors.");
            }
        }
    }

    /**
     * Reads options from the command line, invokes the parser, and then invokes the model checker.
     * @param args
     */
    public static void main(String[] args) {
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY); // this is done so we can terminate if it gets out of hand
        if (args.length == 0) {
            printUsage();
        } else {
            File f = new File(args[0]);
            filename = f.getName();
            if (!f.exists()) {
                System.err.println("Please specify a file that exists.");
                printUsage();
                System.exit(-1);
            }
            System.out.println("Running State Space Explorer for " + filename);
            for (int i = 1; i < args.length; i++) {
                String arg = args[i];
                if ("stop_at_first_error".equalsIgnoreCase(arg)) {
                    stopAtFirstError = true;
                } else if ("no_recursion".equalsIgnoreCase(arg)) {
                    noRecursion = true;
                }
                String[] t = arg.split("=");
                if (t.length == 2) {
                    if ("maxdepth".equalsIgnoreCase(t[0])) {
                        maxdepth = Long.parseLong(t[1]);
                    } else if ("verbosity".equalsIgnoreCase(t[0])) {
                        verbosity = Integer.parseInt(t[1]);
                    } else {
                        System.out.println("unknown option: " + arg);
                    }
                }

            }
            // java StateSpaceExplorer filename.li maxdepth=10000 maxmem=10000
            // create the AST, parse it, and create the complete model, then pass it to the sse
            AstToObjectEngine atoe = new AstToObjectEngine(args[0]);
            try {
                PrintWriter pcs = new PrintWriter(new FileOutputStream(filename + ".pcs"));
                atoe.parse(pcs);
                model = atoe.getModel();
                StateSpaceExplorer sse = new StateSpaceExplorer(model);
                sse.start();
            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
                System.exit(-1);
            }
        }
    }

    private static void printUsage() {
        System.out.println("Usage: ");
        System.out.println("java StateSpaceExplorer filename [verbosity=n] [maxdepth=n] [stop_at_first_error] [no_recursion]");
        System.out.println("When the recursive depth first search algorithm is used, add the switch -Xss2048k to increase the java stack space. ");
        System.out.println("If this is not done, java will run out of stack space after just a couple of thousand recursive calls.");
        System.out.println("Using -Xss2048k allows java to reach a depth of at least 10000 with ease");
        System.out.println("");
        System.out.println("- verbosity default 0 (Allowed values are 0,1,2,3,4, where 4 is the highest)");
        System.out.println("- maxdepth default 10000");
        System.out.println("- stop_at_first_error default false");
        System.out.println("- no_recursion default false");
    }

    public static boolean isStopAtFirstError() {
        return stopAtFirstError;
    }

    public void addAssertionViolation(AssertionViolation av) {
        assertionViolations.put(av.getDepth(), av);
    }

    /**
     * Visualizes an assertion violation.
     *
     * @param violation the assertion violation to make visible.
     */
    private void visualizeAssertionViolation(AssertionViolation violation) {
        System.out.print("AssertionViolation@{");
        System.out.print("pid=" + violation.getAssertion().getPid() + ":[depth=" + violation.getDepth() +";pc=" + violation.getState().getStateRepresentation()[violation.getAssertion().getPid()] + ";statement=" + violation.getAssertion().getDescription() + "]");
        System.out.println("}");
    }

    /**
     * Visualizes the path to a certain state. This state could be a deadlock or an assertion violation (or anything
     * else for that matter) state.
     * It works by looping and asking the current state for its previous state, then loops over that, etc.
     * This will print the trail in reverse order, to prevent us from having to put a huge datastructure into memory.
     * This will not so much visualize the steps taken, but rather the intermediate states that were reached
     * in a walk from beginning to end.
     *
     * @param state the state to visualize the path to.
     */
    private void visualizePath(State state) {
        // go over the previous state for each state
        // see which process was altered to reach that state
        // record the pid and the pc for this step
        // add it first in the LinkedList
        // get the previous state.
        // this was done before, it made traces look better, but they just skipped long sections of loops
        // _todo: this only shows the do-loop for each step. we should show the (pc for) the option we choose
        System.out.println("Writing trail to file");
        while (state != null) {
            trail.println(state.getDepth() + ":" + state);
            state = state.getPreviousState();
        }
        trail.flush();
        trail.close();
    }

    /**
     * Visualizes a deadlock.
     * 
     * @param deadlock the deadlock to visualize.
     */
    private void visualizeDeadlock(Deadlock deadlock) {
        // check each pid, and get the statement for the pc in that pid
        System.out.print("Deadlock@{");
        for (int i = 0; i < model.getNumberOfProcesses(); i++) {
            //for each process, give us the statements for the pc that process currently (in this state) is in
            Statement statement = model.getProcess(i).getStatement(deadlock.getState().getStateRepresentation()[i]);
            System.out.print("pid=" + i + ":[depth=" + deadlock.getDepth() +";pc=" + deadlock.getState().getStateRepresentation()[i] + ";statement=" + statement.getDescription() + "]");
        }
        System.out.println("}");
    }
}
