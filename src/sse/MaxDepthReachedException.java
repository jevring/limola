package sse;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-maj-31 : 21:50:05
 */
public class MaxDepthReachedException extends Throwable {
    public MaxDepthReachedException(long depth) {
        super("Max depth reached: " + depth);
    }
}
