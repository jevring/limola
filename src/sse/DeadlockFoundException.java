package sse;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 1-jun-2007 : 10:44:09
 */
public class DeadlockFoundException extends Throwable {
    private State state;
    private long depth;
    public DeadlockFoundException(State state, long depth) {
        super("Deadlock found at depth " + depth + " : " + state);
        this.state = state;
        this.depth = depth;
    }

    public DeadlockFoundException(Deadlock deadlock) {
        super("Deadlock found at depth " + deadlock.getDepth() + " : " + deadlock.getState());
        this.state = deadlock.getState();
        this.depth = deadlock.getDepth();
    }

    public State getState() {
        return state;
    }

    public long getDepth() {
        return depth;
    }
}
