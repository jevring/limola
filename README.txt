Instructions on how to compile and run the model checker.

All these command should be executed from inside the base dir of the model checker.

Preparations:

1. Make sure you have javac, java and jar in your path (/usr/java/$JAVA_VERSION/bin usually)
2. Make sure antlr-2.7.7.jar (included) is in the base directory (../Limola)
3. Make sure MANIFEST.MF (included) is in the base directory (../Limola)


Compile & run:

1.  Compile: javac -cp antlr-2.7.7.jar -d classes/ src/ast/*.java src/parser/*.java src/sse/*.java
1b. Compile java 1.5 compatibility: javac -cp antlr-2.7.7.jar -d classes/ -target 1.5 src/ast/*.java src/parser/*.java src/sse/*.java
4:  Jar: jar cvfm Limola.jar MANIFEST.MF -C classes .
5:  Run: java -Xms256m -Xmx512m -Xss32m -cp antlr-2.7.7.jar -jar Limola.jar {command line options go here}

--------------------------------------------------------------------------------------------------------

Usage: 
java -Xms256m -Xmx512m -Xss32m -jar Limola.jar filename [verbosity=n] [maxdepth=n] [stop_at_first_error] [no_recursion]

When the recursive depth first search algorithm is used, add the switch -Xss2048k to increase the java stack space. 
If this is not done, java will run out of stack space after just a couple of thousand recursive calls.
Using -Xss2048k allows java to reach a depth of at least 10000.

- verbosity default 0 (Allowed values are 0,1,2,3,4, where 4 is the highest)
- maxdepth default 10000
- stop_at_first_error default false
- no_recursion default false

Note that specifying -Xss32m uses 32 Megabytes of stackspace for java. 
While this is not needed for smaller runs or runs that use 'no_recursion', 
stackspace should be allocated based on how much you need.
Tests show that a recursion depth of around 5000 is possible with default stackspace
10000 requires -Xss2048k
100000 requires at least -Xss8192k

We have run to a depth of 500000 with -Xss32m

The tool will generate filename.pcs and filename.trail files. The filename is the same as the filename of the model.
The .pcs file contains a list of what program counters correspond to what statement in the processes.
The .trail file contains a listing of states that indicate the path to the error, starting at the bottom. 
This file will be empty if no errors were detected.

The state has the following structure:
- The first n shorts represent the program counters for the processes, where n is the number of processes in the model.
- The following m shorts represent the global variables of the model.
- The remaining shorts in the state represent the local variables for the different processes (if any), 
starting with variable 0 from process 0, going up to variable v for process p

--------------------------------------------------------------------------------------------------------

ANTlr:

We use the ANTlr parser generator to create a parser for our grammar.
Since the parser generator is created in preprocessing, and any change to the grammar might make the java
code impossible to compile, we STRONGLY recommend that you do NOT re-generate the parser with ANTlr.

Should this be desired, however, we take no responsibility for breakage of the generated java code.
The ANTlr tool is run as follows:

cd src/parser;
java -cp antlr-2.7.7.jar antlr.Tool limola.g

